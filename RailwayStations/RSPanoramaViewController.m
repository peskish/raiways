//
//  RSPanoramaViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 25.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSPanoramaViewController.h"

@interface RSPanoramaViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation RSPanoramaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
    [self.navigationController.navigationBar setTranslucent:YES];
//    [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"shevrone.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"НАЗАД" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"GothamPro-Light" size:13]];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 100, 32)];
    [backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    negativeSpacer.width = -30;
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer, backButtonItem] animated:NO];
    self.navigationItem.hidesBackButton = YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.webView.hidden = NO;

    UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityView.center = self.webView.center;
    [activityView startAnimating];
    activityView.tag = 100;
    [self.view addSubview:activityView];
    
    self.webView.delegate = self; // Here is the key
    
    NSURL *websiteUrl = [NSURL URLWithString:@"http://dzvr.ru/terminals/files/leningradsky/panorama.html"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [self.webView loadRequest:urlRequest];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.webView.hidden = YES;
    
}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.view viewWithTag:100].hidden = YES;
}


@end
