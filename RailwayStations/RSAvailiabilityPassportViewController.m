//
//  RSAvailiabilityPassportViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 27.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSAvailiabilityPassportViewController.h"
#import "IDMPhotoBrowser.h"
#import "IDMPhoto.h"
#import "RSPhotoCollectionViewCell.h"

@interface RSAvailiabilityPassportViewController () <UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property NSArray *photosArray;
@property NSMutableArray *photosInBrowserArray;
@property IDMPhotoBrowser *photoBrowser;
@property (weak, nonatomic) IBOutlet UIView *photosView;
@property (weak, nonatomic) IBOutlet UIButton *photosButton;
@property (weak, nonatomic) IBOutlet UIButton *availButton;
@end

@implementation RSAvailiabilityPassportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"shevrone.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"Ленинградский вокзал" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"GothamPro-Light" size:13]];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 200, 32)];
    [backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    negativeSpacer.width = -25;
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer, backButtonItem] animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    self.photosArray = [[NSArray alloc]initWithObjects:[UIImage imageNamed:@"a1.jpg"], [UIImage imageNamed:@"a2.jpg"],[UIImage imageNamed:@"a3.jpg"],[UIImage imageNamed:@"a4.jpg"],[UIImage imageNamed:@"a5.jpg"],[UIImage imageNamed:@"a6.jpg"],[UIImage imageNamed:@"a7.jpg"],[UIImage imageNamed:@"a8.jpg"],[UIImage imageNamed:@"a9.jpg"],[UIImage imageNamed:@"a10.jpg"],[UIImage imageNamed:@"a11.jpg"],[UIImage imageNamed:@"a12.jpg"],[UIImage imageNamed:@"a13.jpg"],[UIImage imageNamed:@"a14.jpg"],[UIImage imageNamed:@"a15.jpg"],[UIImage imageNamed:@"a16.jpg"],[UIImage imageNamed:@"a17.jpg"],[UIImage imageNamed:@"a18.jpg"],[UIImage imageNamed:@"a19.jpg"],nil];
    self.photosInBrowserArray = [NSMutableArray new];
    
    for (UIImage *image in self.photosArray)
    {
        IDMPhoto *photoForGallery = [IDMPhoto photoWithImage:image];
        [self.photosInBrowserArray addObject:photoForGallery];
    }
    
    self.photoBrowser = [[IDMPhotoBrowser alloc] initWithPhotos:self.photosInBrowserArray];
    
    [self.collectionView reloadData];
}
- (IBAction)photosButtonTapped:(id)sender {
    self.photosView.hidden = NO;
    [self.photosButton setBackgroundColor:[UIColor whiteColor]];
    [self.photosButton setTitleColor:[UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1] forState:UIControlStateNormal];
    [self.availButton setBackgroundColor:[UIColor clearColor]];
    [self.availButton setTitleColor:[UIColor colorWithRed:0.47 green:0.47 blue:0.47 alpha:1.00] forState:UIControlStateNormal];
}

- (IBAction)availButtonTapped:(id)sender {
    self.photosView.hidden = YES;
    [self.photosButton setBackgroundColor:[UIColor clearColor]];
    [self.photosButton setTitleColor:[UIColor colorWithRed:0.47 green:0.47 blue:0.47 alpha:1.00] forState:UIControlStateNormal];
    [self.availButton setBackgroundColor:[UIColor whiteColor]];
    [self.availButton setTitleColor:[UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1] forState:UIControlStateNormal];

}

//==--------------------------------------------
#pragma mark - CollectionView DELEGATE
//==--------------------------------------------

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return self.photosArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RSPhotoCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    [cell.photosImageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"a%li.jpg",(long)indexPath.item+1]]];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeZero;
    size.width = self.view.frame.size.width / 3;
    size.height = size.width;
    return size;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:self.photosInBrowserArray];
    [browser setInitialPageIndex:indexPath.item];
    [self presentViewController:browser animated:YES completion:nil];
    
}


- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
