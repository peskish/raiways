//
//  RSStationNavigationViewController.h
//  RailwayStations
//
//  Created by Artem Peskishev on 25.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSStationNavigationViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) NSArray *sublocId;

@end
