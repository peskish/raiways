//
//  RSHistoryViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 24.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSHistoryViewController.h"

@interface RSHistoryViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation RSHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"shevrone.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"О вокзале" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"GothamPro-Light" size:13]];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 150, 32)];
    [backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    negativeSpacer.width = -30;
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer, backButtonItem] animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"Строительство ветви\nРязано-Уральской\nжелезной дороги\n\nОбщество Рязанско-Уральской железной дороги в царской России управляло крупнейшей частной дорогой, которая связала 12 густонаселённых губерний. Но выхода в Москву дорога не имела. В связи с этим Правление дороги выступило перед правительством с ходатайством дать разрешение на постройку ветви Павелец — Москва. В мае 1897 года, было получено разрешение Николая II на постройку линии. Постройка новой ветки была завершена в сжатые сроки — на 8,5 месяцев ранее назначенного. Однако построенная дорога не имела вокзала в Москве.\nОткрытие вокзала прошло 1 сентября 1900 года. На вокзале был отслужен молебен с водоосвящением. На торжествах присутствовали главный инженер строительства В. В. Тимофеев, начальники служб, станционный персонал и представители делового мира — будущие грузоотправители."];
    
    [string addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:@"GothamPro" size:22]
                  range:NSMakeRange(0, 53)];
    
    [string addAttribute:NSFontAttributeName
                   value:[UIFont fontWithName:@"GothamPro-Light" size:14]
                   range:NSMakeRange(54, string.length-54)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 1;
    paragraphStyle.lineHeightMultiple = 1.2;
    
    [string addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, string.length)];
    
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.47 green:0.47 blue:0.47 alpha:1.00] range:NSMakeRange(0, string.length)];
    
    [self.textView setAttributedText:string];
    
}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
