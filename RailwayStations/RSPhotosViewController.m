//
//  RSPhotosViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 28.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSPhotosViewController.h"
#import "RSPhotoCollectionViewCell.h"
#import "IDMPhotoBrowser.h"
#import "IDMPhoto.h"

@interface RSPhotosViewController ()
@property NSArray *photosArray;
@property NSMutableArray *photosInBrowserArray;
@property IDMPhotoBrowser *photoBrowser;
@end

@implementation RSPhotosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"shevrone.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"О вокзале" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"GothamPro-Light" size:13]];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 150, 32)];
    [backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    negativeSpacer.width = -30;
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer, backButtonItem] animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    self.photosArray = [[NSArray alloc]initWithObjects:[UIImage imageNamed:@"0.jpg"], [UIImage imageNamed:@"1.jpg"],[UIImage imageNamed:@"2.jpg"],[UIImage imageNamed:@"3.jpg"],[UIImage imageNamed:@"4.jpg"],nil];
    self.photosInBrowserArray = [NSMutableArray new];
    
    for (UIImage *image in self.photosArray)
    {
        IDMPhoto *photoForGallery = [IDMPhoto photoWithImage:image];
        [self.photosInBrowserArray addObject:photoForGallery];
    }
    
    self.photoBrowser = [[IDMPhotoBrowser alloc] initWithPhotos:self.photosInBrowserArray];

}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

//==--------------------------------------------
#pragma mark - CollectionView DELEGATE
//==--------------------------------------------

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return self.photosArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RSPhotoCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    [cell.photosImageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%li.jpg",(long)indexPath.item]]];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeZero;
    size.width = self.view.frame.size.width / 3;
    size.height = size.width;
    return size;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:self.photosInBrowserArray];
    [browser setInitialPageIndex:indexPath.item];
    [self presentViewController:browser animated:YES completion:nil];

}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
