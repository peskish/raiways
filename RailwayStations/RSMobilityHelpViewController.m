//
//  RSMobilityHelpViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 27.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSMobilityHelpViewController.h"
#import "TTTAttributedLabel.h"
#import <MessageUI/MessageUI.h>

@interface RSMobilityHelpViewController () <TTTAttributedLabelDelegate,MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *label;

@end

@implementation RSMobilityHelpViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"shevrone.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"Ленинградский вокзал" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"GothamPro-Light" size:13]];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 200, 32)];
    [backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    
    self.label.enabledTextCheckingTypes = NSTextCheckingAllSystemTypes; // Automatically detect links when the label text is subsequently changed
    self.label.delegate = self;
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    negativeSpacer.width = -25;
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer, backButtonItem] animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    NSString *text = @"Обратитесь в центр содействия за помощью. Для этого вам необходимо позвонить по телефону и записаться на услугу. Звоните не ранее чем за три дня. но не позже чем за 24 часа.\n\nТак же вы можете подать заявку прямо из этого приложения. Для этого просто заполните форму ниже.\n\nТел.: 8-800-510-11-11\ne-mail: pmkursk@comсor.ru;\npmkursk@gmail.com";
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 1;
    paragraphStyle.lineHeightMultiple = 1.1;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, text.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"GothamPro-Light" size:15] range:NSMakeRange(0, text.length)];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.47 green:0.47 blue:0.47 alpha:1.00] range:NSMakeRange(0, text.length)];
    self.label.attributedText = attributedString;
    self.label.linkAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:0.47 green:0.47 blue:0.47 alpha:1.00]};

    
    NSRange mail1 = [self.label.text rangeOfString:@"pmkursk@comсor.ru"];
    [self.label addLinkToURL:[NSURL URLWithString:@"http://1.com"] withRange:mail1];
    NSRange mail2 = [self.label.text rangeOfString:@"pmkursk@gmail.com"];
    [self.label addLinkToURL:[NSURL URLWithString:@"http://2.com"] withRange:mail2];
    
    self.label.textColor = [UIColor colorWithRed:0.47 green:0.47 blue:0.47 alpha:1.00];
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        
        if ([[url absoluteString] isEqualToString:@"http://1.com"])
        {
            [mailCont setToRecipients:[NSArray arrayWithObject:@"pmkursk@comсor.ru"]];
        } else if ([[url absoluteString] isEqualToString:@"http://2.com"])
        {
            [mailCont setToRecipients:[NSArray arrayWithObject:@"pmkursk@gmail.com"]];
        }
        
        [self presentModalViewController:mailCont animated:YES];
    }
    
    
    // Then implement the delegate method
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissModalViewControllerAnimated:YES];
}



- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (IBAction)callButton:(id)sender {
    
    NSString *phoneNumber = [@"tel://" stringByAppendingString:@"8(800)510-11-11"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

@end
