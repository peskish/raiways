//
//  RSSimpleTextTableViewCell.m
//  RailwayStations
//
//  Created by Artem Peskishev on 24.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSSimpleTextTableViewCell.h"
#define SEPARATOR_HEIGHT 0.5

@implementation RSSimpleTextTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0,64,self.frame.size.width,SEPARATOR_HEIGHT)];
    [separatorView setBackgroundColor:[UIColor colorWithRed:0.365 green:0.365 blue:0.365 alpha:1]];
    [self addSubview:separatorView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
