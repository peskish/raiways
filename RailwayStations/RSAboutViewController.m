//
//  RSAboutViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 24.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSAboutViewController.h"
#import "RSSimpleTextTableViewCell.h"
#import "RSHistoryViewController.h"
#import "RSContactsViewController.h"
#import "RSHowToArriveViewController.h"
#import "RSHowToLeaveViewController.h"
#import "RSPanoramaViewController.h"
#import "RSPhotosViewController.h"

@interface RSAboutViewController ()
@property (strong,nonatomic) NSArray *arrayOfItems;
@end

@implementation RSAboutViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"shevrone.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"Ленинградский вокзал" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"GothamPro-Light" size:13]];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 200, 32)];
    [backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    negativeSpacer.width = -15;
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer, backButtonItem] animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"AboutItems" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:jsonPath];
    NSError *error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    self.arrayOfItems = json[@"items"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_background.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setTranslucent:NO];

    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayOfItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // dequeue a custom table view cell subclass
    RSSimpleTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"simpleTextCell"
                                                                           forIndexPath:indexPath];
    
    NSDictionary *menuItemDict = self.arrayOfItems[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.titleLabel setText:[menuItemDict valueForKey:@"name"]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    switch (indexPath.row) {
        case 0:
        {
            RSHistoryViewController * vc = (RSHistoryViewController *)[sb instantiateViewControllerWithIdentifier:@"historyVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
            case 1:
        {
            RSPhotosViewController * vc = (RSPhotosViewController *)[sb instantiateViewControllerWithIdentifier:@"photosVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 2:
        {
            RSPanoramaViewController * vc = (RSPanoramaViewController *)[sb instantiateViewControllerWithIdentifier:@"panoramaVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 3:
        {
            RSContactsViewController * vc = (RSContactsViewController *)[sb instantiateViewControllerWithIdentifier:@"contactsVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 4:
        {
            RSHowToArriveViewController * vc = (RSHowToArriveViewController *)[sb instantiateViewControllerWithIdentifier:@"howToArriveVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 5:
        {
            RSHowToLeaveViewController * vc = (RSHowToLeaveViewController *)[sb instantiateViewControllerWithIdentifier:@"howToLeaveVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }

            
        default:
            break;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
