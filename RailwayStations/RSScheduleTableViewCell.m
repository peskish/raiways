//
//  RSScheduleTableViewCell.m
//  RailwayStations
//
//  Created by Artem Peskishev on 27.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSScheduleTableViewCell.h"
#define SEPARATOR_HEIGHT 0.5

@implementation RSScheduleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0,79,self.frame.size.width,SEPARATOR_HEIGHT)];
    [separatorView setBackgroundColor:[UIColor colorWithRed:0.365 green:0.365 blue:0.365 alpha:1]];
    [self addSubview:separatorView];
}

-(void)setCellWithDict:(NSDictionary *)dict
{
    self.dictWithInfo = dict;
    
    self.destinLabel.text = dict[@"EndStation"];
    self.fromLabel.text = dict[@"StartStation"];
    self.trainNoLabel.text = dict[@"TrainNumber"];
    self.railNoLabel.text = dict[@"EvTrackNumber"];
    
    if ([dict[@"EvTrackNumber"] isEqualToString:@"0"])
    {
        self.railNoLabel.text = @"-";
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [formatter dateFromString:dict[@"EvRecTime"]];
    [formatter setDateFormat:@"HH:mm"];
    NSString *newDate = [formatter stringFromDate:date];
    
    self.timeLabel.text = newDate;
    self.scheduleButton.selected = NO;

    NSDictionary *schedDict = [[NSUserDefaults standardUserDefaults]objectForKey:@"scheduledTrain"];
    if (schedDict)
    {
        if ([schedDict[@"TrainNumber"] isEqualToString:dict[@"TrainNumber"]])
        {
            self.scheduleButton.selected = YES;
        }
    }
    
    self.starButton.selected = NO;
    NSDictionary *staredDict = [[NSUserDefaults standardUserDefaults]objectForKey:@"lookingTrackForTrain"];
    if (staredDict)
    {
        if ([staredDict[@"TrainNumber"] isEqualToString:dict[@"TrainNumber"]])
        {
            self.starButton.selected = YES;
        }
    }
}
- (IBAction)starButtonTapped:(id)sender {
    NSDictionary *dict;
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"lookingTrackForTrain"])
    {
        dict = [[NSUserDefaults standardUserDefaults]objectForKey:@"lookingTrackForTrain"];
    }
    if ([dict[@"TrainNumber"] isEqualToString:self.dictWithInfo[@"TrainNumber"]])
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lookingTrackForTrain"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"showAlertForTrain" object:self.dictWithInfo];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lookingTrackForTrain"];
        [[NSUserDefaults standardUserDefaults] setObject:self.dictWithInfo forKey:@"lookingTrackForTrain"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"showAlertForTrain" object:self.dictWithInfo];

        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Внимание" message:@"Вы получите уведомление как только объявят путь для этого поезда" delegate:self cancelButtonTitle:@"Ок" otherButtonTitles: nil];
        [alertView show];
    }

}

- (IBAction)scheduleButtonTapped:(id)sender
{
    [[NSUserDefaults standardUserDefaults]setObject:self.dictWithInfo forKey:@"scheduledTrain"];
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"uid"]];
//        if ([uid isEqualToString:uidtodelete])
//        {
//            //Cancelling local notification
//            [app cancelLocalNotification:oneEvent];
//            break;
//        }
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [formatter dateFromString:self.dictWithInfo[@"EvRecTime"]];
    
    NSTimeInterval secondsBetween = [date timeIntervalSinceDate:[NSDate date]];
    
    NSString *stringToSet = [NSString stringWithFormat:@"До поезда  №%@ осталось " ,self.dictWithInfo[@"TrainNumber"]];
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:secondsBetween-15*60];
    localNotification.alertBody = [stringToSet stringByAppendingString:@"15 минут"];
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"scheduleTrain" object:nil];
}

@end
