//
//  RSReviewViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 25.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSReviewViewController.h"
#import <MessageUI/MessageUI.h>

@interface RSReviewViewController () <MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottomConstraint;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (nonatomic) NSInteger rating;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sendButtonTopConstraint;
@property (weak, nonatomic) IBOutlet UIButton *addPhotoButton;
@property (nonatomic, strong) UIImageView *photoImageView;
@end

@implementation RSReviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"shevrone.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"Ленинградский вокзал" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"GothamPro-Light" size:13]];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 200, 32)];
    [backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    negativeSpacer.width = -25;
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer, backButtonItem] animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.label1.text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 1;
    paragraphStyle.lineHeightMultiple = 1.1;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.label1.text.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"GothamPro-Light" size:15] range:NSMakeRange(0, self.label1.text.length)];
    self.label1.attributedText = attributedString;
    
    attributedString = [[NSMutableAttributedString alloc] initWithString:self.label2.text];
    paragraphStyle.lineSpacing = 1;
    paragraphStyle.lineHeightMultiple = 1.1;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.label2.text.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"GothamPro-Light" size:15] range:NSMakeRange(0, self.label2.text.length)];
    self.label2.attributedText = attributedString;
    
    [self observeKeyboard];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)starButtonTapped:(id)sender {
    UIButton *starButton = sender;
    self.rating = starButton.tag;
    for (UIButton *buton in self.contentView.subviews)
    {
        if (buton.tag)
        {
            if (buton.tag  > starButton.tag)
            {
                buton.selected = NO;
            } else {
                buton.selected = YES;
            }
        }
            
    }
}

- (IBAction)addPhotoButtonTapped:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:nil delegate:self cancelButtonTitle:@"Выбрать из галереи" otherButtonTitles:@"Сделать фото", nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
            break;
        }
        case 0:
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:picker animated:YES completion:NULL];
            break;
        }
        default:
            break;
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    self.photoImageView = [[UIImageView alloc]initWithImage:chosenImage];
    self.photoImageView.frame = CGRectMake(0, self.addPhotoButton.frame.origin.y, 100, 100);
    self.photoImageView.center = CGPointMake(self.addPhotoButton.center.x, self.photoImageView.center.y);
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.clipsToBounds = YES;
    self.sendButtonTopConstraint.constant = 90;
    [self.contentView addSubview:self.photoImageView];
    self.addPhotoButton.hidden = YES;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)observeKeyboard
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

//=====================================================
#pragma mark - MAIL
//=====================================================
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)sendButtonTapped:(id)sender
{
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;

        [mailCont setToRecipients:[NSArray arrayWithObject:@"treatmentlist@dzvr.ru"]];
        [mailCont setSubject:[NSString stringWithFormat:@"Отзыв о Ленинградском вокзале"]];

        if (self.photoImageView)
        {
            NSData *jpegData = UIImageJPEGRepresentation(self.photoImageView.image, 1.0);
            NSString *fileName = @"test";
            fileName = [fileName stringByAppendingPathExtension:@"jpeg"];
            [mailCont addAttachmentData:jpegData mimeType:@"image/jpeg" fileName:fileName];
        }
        NSString *ratingString = @"Рейтинг: ";
        for (int i = 0 ; i < self.rating ; i++)
        {
            ratingString = [ratingString stringByAppendingString:@"★"];
        }
        
        NSString *bodyString = self.textView.text;
        
        
        [mailCont setMessageBody:[NSString stringWithFormat:@"%@\n\n%@",ratingString,bodyString] isHTML:NO];

        
        [self presentModalViewController:mailCont animated:YES];
    }

}


//=====================================================
#pragma mark - KEYBOARD
//=====================================================

- (void) keyboardDidShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    self.scrollViewBottomConstraint.constant = kbRect.size.height;
}

- (void) keyboardWillBeHidden:(NSNotification *)notification
{
    self.scrollViewBottomConstraint.constant = 0;

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}


@end
