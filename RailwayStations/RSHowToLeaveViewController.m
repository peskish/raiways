//
//  RSHowToLeaveViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 25.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSHowToLeaveViewController.h"

@interface RSHowToLeaveViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UILabel *label4;
@property (weak, nonatomic) IBOutlet UILabel *label5;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConstraint;

@end

@implementation RSHowToLeaveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"shevrone.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"О вокзале" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"GothamPro-Light" size:13]];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 150, 32)];
    [backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    negativeSpacer.width = -30;
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer, backButtonItem] animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.label1.text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 1;
    paragraphStyle.lineHeightMultiple = 1.1;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.label1.text.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"GothamPro-Light" size:15] range:NSMakeRange(0, self.label1.text.length)];
    self.label1.attributedText = attributedString;
    
    attributedString = [[NSMutableAttributedString alloc] initWithString:self.label2.text];
    paragraphStyle.lineSpacing = 1;
    paragraphStyle.lineHeightMultiple = 1.1;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.label2.text.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"GothamPro-Light" size:15] range:NSMakeRange(0, self.label2.text.length)];
    self.label2.attributedText = attributedString;
    
    attributedString = [[NSMutableAttributedString alloc] initWithString:self.label3.text];
    paragraphStyle.lineSpacing = 1;
    paragraphStyle.lineHeightMultiple = 1.1;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.label3.text.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"GothamPro-Light" size:15] range:NSMakeRange(0, self.label3.text.length)];
    self.label3.attributedText = attributedString;
    
    attributedString = [[NSMutableAttributedString alloc] initWithString:self.label4.text];
    paragraphStyle.lineSpacing = 1;
    paragraphStyle.lineHeightMultiple = 1.1;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.label4.text.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"GothamPro-Light" size:15] range:NSMakeRange(0, self.label4.text.length)];
    self.label4.attributedText = attributedString;
    
    attributedString = [[NSMutableAttributedString alloc] initWithString:self.label5.text];
    paragraphStyle.lineSpacing = 1;
    paragraphStyle.lineHeightMultiple = 1.1;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.label5.text.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"GothamPro-Light" size:15] range:NSMakeRange(0, self.label5.text.length)];
    self.label5.attributedText = attributedString;
    
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(self.label5.frame));
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.contentViewHeightConstraint.constant = CGRectGetMaxY(self.label5.frame);
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(self.label5.frame));
}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
