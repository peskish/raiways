//
//  RSSearchStationViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 23.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSSearchStationViewController.h"
#import "RSStationTableViewCell.h"
#import "TECustomTextField.h"

@interface RSSearchStationViewController ()
@property (nonatomic, strong) NSArray *arrayOfStations;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dividerHeightConstraint;
@property BOOL isSearch;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet TECustomTextField *textField;
@property (nonatomic, strong) NSArray *filteredArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;
@end

@implementation RSSearchStationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.isSearch = NO;
    
    self.filteredArray = [NSMutableArray new];
    
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"Stations" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:jsonPath];
    NSError *error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    self.dividerHeightConstraint.constant = 1/[UIScreen mainScreen].scale;

    self.textField.delegate = self;
    
    self.arrayOfStations = json[@"items"];
    
    [self observeKeyboard];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isSearch) return self.filteredArray.count;
    return self.arrayOfStations.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // dequeue a custom table view cell subclass
    RSStationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"stationCell"
                                                                   forIndexPath:indexPath];
    
    NSDictionary *stationDict = self.arrayOfStations[indexPath.row];
    
    if (self.isSearch) stationDict = self.filteredArray[indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    [cell setCell:stationDict];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"showRailwayMain" sender:self];
}


- (IBAction)textfieldEditingChanged:(id)sender {
    UITextField *textField = sender;
    if (textField.text.length == 0)
    {
        self.isSearch = NO;
        [self.tableView reloadData];
    } else {
        self.isSearch = YES;
        NSMutableArray *arr = [NSMutableArray new];
        for (NSDictionary* dict in self.arrayOfStations)
        {
            NSString *statName = [[NSString stringWithFormat:@"%@",dict[@"name"]] lowercaseString];
            NSString *searchStr = [textField.text lowercaseString];
            
            if ([statName rangeOfString:searchStr].location == NSNotFound) {
            } else {
                [arr addObject:dict];
            }

        }
        self.filteredArray = arr;
        [self.tableView reloadData];
    }
}

//=====================================================
#pragma mark - KEYBOARD
//=====================================================

- (void)observeKeyboard
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void) keyboardDidShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    self.tableViewBottomConstraint.constant = kbRect.size.height;
}

- (void) keyboardWillBeHidden:(NSNotification *)notification
{
    self.tableViewBottomConstraint.constant = 0;
    
}


-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

@end
