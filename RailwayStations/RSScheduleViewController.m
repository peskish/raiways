//
//  RSScheduleViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 27.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSScheduleViewController.h"
#import "AFNetworking.h"
#import "RSScheduleTableViewCell.h"

@interface RSScheduleViewController ()
@property (nonatomic, strong) NSArray *scheduleArray;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UILabel *scheduledTrainLabel;
@property (weak, nonatomic) IBOutlet UIView *scheduledTrainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scheduledTrainViewHeightConstant;
@property (weak, nonatomic) IBOutlet UIButton *departureButton;
@property (weak, nonatomic) IBOutlet UIButton *arriveButton;

@property BOOL isShowingDeparture;
@property BOOL isSearch;
@property NSArray *filteredArray;
@end

@implementation RSScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isShowingDeparture = YES;
    
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"shevrone.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"Ленинградский вокзал" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"GothamPro-Light" size:13]];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 200, 32)];
    [backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    
    
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    negativeSpacer.width = -25;
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer, backButtonItem] animated:NO];
    self.navigationItem.hidesBackButton = YES;
    [self loadSchedule];
    
    [self scheduleTrain];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(scheduleTrain) name:@"scheduleTrain" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(lookingTrackForTrain:) name:@"showAlertForTrain" object:nil];

}
- (IBAction)showDeparture:(id)sender {
    if (!self.isShowingDeparture)
    {
        [self.departureButton setBackgroundColor:[UIColor colorWithRed:0.77 green:0.07 blue:0.21 alpha:1.00]];
        [self.arriveButton setBackgroundColor:[UIColor colorWithRed:0.57 green:0.09 blue:0.19 alpha:1.00]];
        [self loadSchedule];
    }
    
    [self.view endEditing:YES];
    
    self.isShowingDeparture = YES;
}
- (IBAction)showArrive:(id)sender {
    
    if (self.isShowingDeparture)
    {
        [self loadScheduleForArrive];
        [self.departureButton setBackgroundColor:[UIColor colorWithRed:0.57 green:0.09 blue:0.19 alpha:1.00]];
        [self.arriveButton setBackgroundColor:[UIColor colorWithRed:0.77 green:0.07 blue:0.21 alpha:1.00]];
    }
    [self.view endEditing:YES];

    
    self.isShowingDeparture = NO;
}

-(void)lookingTrackForTrain:(NSNotification *)notification
{
    [self.tableview reloadData];
}

- (void)scheduleTrain
{
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults]objectForKey:@"scheduledTrain"];
    if (dict)
    {
        NSString *trainNumber = dict[@"TrainNumber"];

        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [formatter dateFromString:dict[@"EvRecTime"]];

        for (NSDictionary *trainDict in self.scheduleArray)
        {
            if ([dict[@"TrainNumber"] isEqualToString:trainDict[@"TrainNumber"]])
            {
                date = [formatter dateFromString:trainDict[@"EvRecTime"]];
                break;

            }
        }
        
        NSTimeInterval secondsBetween = [date timeIntervalSinceDate:[NSDate date]];
        
        NSInteger hours = secondsBetween/3600;
        
        NSInteger minutes = (secondsBetween - hours * 3600)/60;
        
        NSString *stringToSet = [NSString stringWithFormat:@"До поезда  №%@ осталось " ,trainNumber];
        
        if (hours > 0)
        {
            stringToSet = [stringToSet stringByAppendingString:[NSString stringWithFormat:@"%li %@ ",(long)hours,[self format:hours forWords:@[@"час",@"часа",@"часов"]]]];
        }
        
         if (minutes > 0)
         {
            stringToSet = [stringToSet stringByAppendingString:[NSString stringWithFormat:@"%li %@",(long)minutes,[self format:minutes forWords:@[@"минута",@"минуты",@"минут"]]]];
         }
        
        [self.scheduledTrainLabel setText:stringToSet];
        self.scheduledTrainView.hidden = NO;
        self.scheduledTrainViewHeightConstant.constant = 60;
        


    } else {
        self.scheduledTrainView.hidden = YES;
        self.scheduledTrainViewHeightConstant.constant = 0;

    }
    [self.tableview reloadData];
}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)removeSchedule:(id)sender
{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"scheduledTrain"];
    [self scheduleTrain];
}

-(void)loadSchedule
{
    self.tableview.hidden = YES;
    NSString *urlAsString = @"http://cms.indigointeractive.ru/rasp/json.php?terminalid=61";
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    NSLog(@"%@", urlAsString);
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (error) {
        } else {
            self.scheduleArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableview reloadData];
                [self scheduleTrain];
                self.tableview.hidden = NO;

            });
        }
    }];
}

-(void)loadScheduleForArrive
{
    self.tableview.hidden = YES;

    NSString *urlAsString = @"http://cms.indigointeractive.ru/rasp/json.php?terminalid=61&route=departure";
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    NSLog(@"%@", urlAsString);
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (error) {
        } else {
            self.scheduleArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableview.hidden = NO;

                [self.tableview reloadData];
                [self scheduleTrain];
                
            });
        }
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isSearch) return self.filteredArray.count;
    return self.scheduleArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // dequeue a custom table view cell subclass
    RSScheduleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"scheduleCell"
                                                                      forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    NSDictionary *menuItemDict = self.scheduleArray[indexPath.row];
    if (self.isSearch) menuItemDict = self.filteredArray[indexPath.row];
    
    [cell setCellWithDict:menuItemDict];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


- (NSString *)format:(NSInteger)daysCount forWords:(NSArray *)words
{
    NSString *sEnding;
    NSArray *aEndings = words;
    daysCount = daysCount % 100;
    if (daysCount>=11 && daysCount<=19) {
        sEnding=aEndings[2];
    }
    else {
        int i = daysCount % 10;
        switch (i)
        {
            case (1): sEnding = aEndings[0]; break;
            case (2):
            case (3):
            case (4): sEnding = aEndings[1]; break;
            default: sEnding = aEndings[2];
        }
    }
    return sEnding;
}

- (IBAction)textfieldEditingChanged:(id)sender {
    UITextField *textField = sender;
    if (textField.text.length == 0)
    {
        self.isSearch = NO;
        [self.tableview reloadData];
    } else {
        self.isSearch = YES;
        NSMutableArray *arr = [NSMutableArray new];
        for (NSDictionary* trainDict in self.scheduleArray)
        {
            NSString *trainNo = [[NSString stringWithFormat:@"%@",trainDict[@"TrainNumber"]] lowercaseString];
            NSString *endStat = [[NSString stringWithFormat:@"%@",trainDict[@"EndStation"]] lowercaseString];
            NSString *searchStr = [textField.text lowercaseString];

            if ([endStat rangeOfString:searchStr].location == NSNotFound) {
            } else {
                [arr addObject:trainDict];
            }
            
            if ([trainNo rangeOfString:searchStr].location == NSNotFound)
            {
            }else {
                [arr addObject:trainDict];
            }
        }
        self.filteredArray = arr;
        [self.tableview reloadData];
    }
}



-(void)textFieldDidEndEditing:(UITextField *)textField
{

    

}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
