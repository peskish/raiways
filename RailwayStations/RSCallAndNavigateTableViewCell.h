//
//  RSCallAndNavigateTableViewCell.h
//  RailwayStations
//
//  Created by Artem Peskishev on 24.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSCallAndNavigateTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) NSString *phoneNumber;
@end
