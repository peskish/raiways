//
//  RSMenuItemWithImageTableViewCell.h
//  RailwayStations
//
//  Created by Artem Peskishev on 24.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSMenuItemWithImageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *menuIcon;
@property (weak, nonatomic) IBOutlet UILabel *menuTitle;
@property (weak, nonatomic) IBOutlet UIImageView *grayChevroneImageView;

-(void)setCellWithDict:(NSDictionary *)dict;

@end
