//
//  RSStationTableViewCell.m
//  RailwayStations
//
//  Created by Artem Peskishev on 23.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSStationTableViewCell.h"
#define SEPARATOR_HEIGHT 0.5

@implementation RSStationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0,63,self.frame.size.width,SEPARATOR_HEIGHT)];
    [separatorView setBackgroundColor:[UIColor colorWithRed:0.365 green:0.365 blue:0.365 alpha:1]];
    [self addSubview:separatorView];}

-(void)setCell:(NSDictionary *)dict
{
    self.nameLabel.text = [dict valueForKey:@"name"];
    self.descriptLabel.text = [dict valueForKey:@"metro"];
}

@end
