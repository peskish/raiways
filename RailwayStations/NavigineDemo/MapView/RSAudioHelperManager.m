//
//  RSAudioHelperManager.m
//  RailwayStations
//
//  Created by Artem Peskishev on 11.04.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSAudioHelperManager.h"

@interface RSAudioHelperManager()


@end
@implementation RSAudioHelperManager
@synthesize sound1;

+ (RSAudioHelperManager*)sharedManager
{
    static RSAudioHelperManager *__sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[RSAudioHelperManager alloc] init];
    });
    return __sharedInstance;
}

-(void)play:(NSString *)cornerType
{
    NSURL *soundURL = [[NSBundle mainBundle] URLForResource:cornerType
                                              withExtension:@"mp3"];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, self.sound1);
    AudioServicesAddSystemSoundCompletion (self.sound1,NULL,NULL,theAudioServicesSystemSoundCompletionProc,(__bridge void*)self);
    AudioServicesPlaySystemSound(self.sound1);
}

static void theAudioServicesSystemSoundCompletionProc (SystemSoundID  sound1, void *myself) {
    NSLog(@"Audio callback");
    AudioServicesRemoveSystemSoundCompletion (sound1);
    AudioServicesDisposeSystemSoundID(sound1);

}

@end
