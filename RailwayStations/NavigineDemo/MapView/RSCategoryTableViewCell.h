//
//  RSCategoryTableViewCell.h
//  RailwayStations
//
//  Created by Artem Peskishev on 31.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Category.h"

@interface RSCategoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

-(void)setCellWith:(Categories *)cat;

@end
