//
//  RSVenuePointsViewController.h
//  RailwayStations
//
//  Created by Artem Peskishev on 31.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapHelper.h"

@interface RSVenueCategoriesViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,MapViewDelegate>

@property NSArray *venuePoints;
@property NSArray *categories;
@end
