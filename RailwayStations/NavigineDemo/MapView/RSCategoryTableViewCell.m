//
//  RSCategoryTableViewCell.m
//  RailwayStations
//
//  Created by Artem Peskishev on 31.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSCategoryTableViewCell.h"
#import "UIImageView+WebCache.h"

@implementation RSCategoryTableViewCell

#define SEPARATOR_HEIGHT 0.5


- (void)awakeFromNib {
    [super awakeFromNib];
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0,49,500,SEPARATOR_HEIGHT)];
    [separatorView setBackgroundColor:[UIColor colorWithRed:0.365 green:0.365 blue:0.365 alpha:1]];
    [self addSubview:separatorView];
    
}


-(void)setCellWith:(Categories *)cat
{
//    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:cat.image]];
    [self.nameLabel setText:cat.nameRu];
    
    switch (cat.id) {
            case 0:
            [self.iconImageView setImage:[UIImage imageNamed:@"userLocationIcon"]];
             break;
            case 999:
            [self.iconImageView setImage:[UIImage imageNamed:@"vip"]];
            break;
        case 1:
//            [self.nameLabel setText:@"Кафе и рестораны"];
            [self.iconImageView setImage:[UIImage imageNamed:@"cafeIcon"]];
            break;
        case 2:
//            [self.nameLabel setText:@"Напитки и сладости"];
            [self.iconImageView setImage:[UIImage imageNamed:@"cafeIcon"]];
            break;
        case 3:
//            [self.nameLabel setText:@"Одежда и мода"];
            [self.iconImageView setImage:[UIImage imageNamed:@"storeIcon"]];
            break;
        case 4:
//            [self.nameLabel setText:@"Парфюмерия"];
            [self.iconImageView setImage:[UIImage imageNamed:@"storeIcon"]];
            break;
        case 5:
//            [self.nameLabel setText:@"Драгоценности"];
            [self.iconImageView setImage:[UIImage imageNamed:@"storeIcon"]];
            break;
        case 6:
//            [self.nameLabel setText:@"Сувениры и подарки"];
            [self.iconImageView setImage:[UIImage imageNamed:@"storeIcon"]];
            break;
        case 7:
//            [self.nameLabel setText:@"Электроника"];
            [self.iconImageView setImage:[UIImage imageNamed:@"storeIcon"]];
            break;
        case 8:
//            [self.nameLabel setText:@"Товары в дорогу"];
            [self.iconImageView setImage:[UIImage imageNamed:@"storeIcon"]];
            break;
        case 9:
//            [self.nameLabel setText:@"Аптеки"];
            [self.iconImageView setImage:[UIImage imageNamed:@"medIcon"]];
            break;
        case 10:
//            [self.nameLabel setText:@"Туалеты"];
            [self.iconImageView setImage:[UIImage imageNamed:@"toiletsIcon"]];
            break;
        case 11:
//            [self.nameLabel setText:@"Камеры хранения"];
            [self.iconImageView setImage:[UIImage imageNamed:@"lockerIcon"]];
            break;
        case 12:
//            [self.nameLabel setText:@"Кассы"];
            [self.iconImageView setImage:[UIImage imageNamed:@"ticketsIcon"]];
            break;
        case 13:
//            [self.nameLabel setText:@"Залы ожидания"];
            [self.iconImageView setImage:[UIImage imageNamed:@"waitingIcon"]];
            break;
        case 14:
            //            [self.nameLabel setText:@"Залы ожидания"];
            [self.iconImageView setImage:[UIImage imageNamed:@"atmIcon"]];
            break;
        case 15:
            //            [self.nameLabel setText:@"Магазины"];
            [self.iconImageView setImage:[UIImage imageNamed:@"storeIcon"]];
            break;
        case 16:
            //            [self.nameLabel setText:@"Военный комендант"];
            [self.iconImageView setImage:[UIImage imageNamed:@"comendantIcon"]];
            break;
        case 17:
            //            [self.nameLabel setText:@"Рестораны"];
            [self.iconImageView setImage:[UIImage imageNamed:@"restIcon"]];
            break;
        case 18:
            //            [self.nameLabel setText:@"Сервис"];
            [self.iconImageView setImage:[UIImage imageNamed:@"servicesIcon"]];
            break;
        case 19:
            //            [self.nameLabel setText:@"Интернет"];
            [self.iconImageView setImage:[UIImage imageNamed:@"internetIcon"]];
            break;
            case 20:
            [self.iconImageView setImage:[UIImage imageNamed:@"sapsanIcon"]];
            break;
        default:
            break;
    }
    
    self.iconImageView.image = [self.iconImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.iconImageView setTintColor:[UIColor colorWithRed:0.47 green:0.47 blue:0.47 alpha:1.00]];

}

@end
