//
//  RSVenuePointsViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 01.04.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSVenuePointsViewController.h"
#import "RSVenueTableViewCell.h"
#import "MapViewController.h"


@interface RSVenuePointsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) MapHelper *mapHelper;
@property (weak, nonatomic) IBOutlet UILabel *topBarLabel;

@end

Venue *venues1;
NSString *type1;

@implementation RSVenuePointsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.topBarLabel setText:self.category.nameRu];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.mapHelper = [MapHelper sharedInstance];
    self.mapHelper.venueDelegate = self;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.venuePoints.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // dequeue a custom table view cell subclass
    RSVenueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"venueCell"
                                                                    forIndexPath:indexPath];
    Venue *cat = self.venuePoints[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell.nameLabel setText:cat.name];
    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    type1 = @"route";
    venues1 = self.venuePoints[indexPath.row];
    
    MapViewController *mapVC = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    [mapVC showVenue];
    [self.navigationController popToViewController:mapVC animated:YES];
    
}

- (Venue *)routeToPlace {
    return venues1;
}

- (NSString *)showType {
    return type1;
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
