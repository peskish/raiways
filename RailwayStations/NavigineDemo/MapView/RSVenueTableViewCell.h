//
//  RSVenueTableViewCell.h
//  RailwayStations
//
//  Created by Artem Peskishev on 01.04.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSVenueTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
