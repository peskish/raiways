//
//  RSCategoryContentViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 29.04.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSCategoryContentViewController.h"

@interface RSCategoryContentViewController ()
@property (weak, nonatomic) IBOutlet UILabel *contentTitleLable;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation RSCategoryContentViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = false;
    self.textView.scrollEnabled = false;

    [self.contentTitleLable setText:@"VIP сопровождение"];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:self.textView.text];
    
    [string addAttribute:NSFontAttributeName
                   value:[UIFont fontWithName:@"GothamPro" size:20]
                   range:NSMakeRange(0, 172)];
    
    [string addAttribute:NSFontAttributeName
                   value:[UIFont fontWithName:@"GothamPro-Light" size:14]
                   range:NSMakeRange(173, 312)];
    
    [string addAttribute:NSFontAttributeName
                   value:[UIFont fontWithName:@"GothamPro" size:22]
                   range:NSMakeRange(485, 45)];
    
    [string addAttribute:NSFontAttributeName
                   value:[UIFont fontWithName:@"GothamPro-Light" size:14]
                   range:NSMakeRange(522, 825)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 1;
    paragraphStyle.lineHeightMultiple = 1.2;
    
    [string addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, string.length)];
    
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, string.length)];
    
    [self.textView setAttributedText:string];

    self.textView.dataDetectorTypes = UIDataDetectorTypeAll;

    self.textView.scrollEnabled = true;

}

- (void)viewDidLayoutSubviews {
    [self.textView setContentOffset:CGPointZero animated:NO];
}

@end
