//  MapView
//  NavigineDemo
//
//  Created by Administrator on 7/14/14.
//  Copyright (c) 2014 Navigine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MapViewController.h"
#import "NavigineSDK.h"
#import "NavigineManager.h"
#import "TTSwitch.h"
#import "SVProgressHUD.h"
#import "RSVenueCategoriesViewController.h"
#import "UIImageView+WebCache.h"
#import "RSAudioHelperManager.h"

@interface MapViewController(){
    NSLock *loadArchiveLock;
    NSArray *loadedLocations;

    
  NSUInteger logsTotalCount;
  NSUInteger logsCount;
  BOOL enableFollow;
  BOOL isRoutingNow;
  int sublocationId;
  NSInteger error4count;
  CGFloat zoomScale;
  
  MapPin *currentPin;//ввеньюс-кнопка, на который нажали
    
  NSMutableArray *pins;// массив с MapPin'ами
  
  //view that contains PressPin & MapPins than shouldn't zoom
  UIView *viewWithNoZoom;
  
  UIView *routeErrorView;
  NSTimer *errorViewTimer;
  
  NSMutableArray *routeArray;
  CAShapeLayer   *routeLayer;
  
  CAShapeLayer   *processLayer;
  
  UIBezierPath   *uipath;
  UIBezierPath *processPath;
  PressPin *pin; // то, что после лонг-тапа появляется
    Venue *selectedVenue;
  PointOnMap routePoint;
  
  PositionOnMap *current;
  
  CGPoint originOffset;
  
  double yawByIos;
  double lineWith;
  NavigationResults res;
  BOOL centeredAroundCurrent;
  NSTimer *dynamicModeTimer;
  ErrorViewType errorViewType;
  CGPoint translatedPoint;
}
@property (nonatomic) DistanceType distanceType;
@property (nonatomic) RouteType routeType;

@property (nonatomic, strong) NCImage *image;
@property (nonatomic, strong) LoaderHelper *loaderHelper;
@property (nonatomic, strong) MapHelper *mapHelper;
@property (nonatomic, strong) NavigineManager *navigineManager;
@property (nonatomic, strong) DebugHelper *debugHelper;
@property (weak, nonatomic) IBOutlet RSCustomScrollView *contentScrollView;
@property (weak, nonatomic) IBOutlet UIView *topBar;
@property (weak, nonatomic) IBOutlet UIButton *showVenuesButton;
@property (nonatomic, strong) TTSwitch *mapListSwitch;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (nonatomic,strong) NSMutableArray *cornerVertexes;
@property NSArray *oldPath;
@property BOOL isAudioEnabled;
@property BOOL isShowCripple;
@end

@implementation MapViewController
//- (IBAction)showVenues:(id)sender {
////    [self performSegueWithIdentifier:@"showVenueList" sender:self];
//    [self.contentScrollView setContentOffset:CGPointMake(self.contentScrollView.frame.size.width, 0) animated:YES];
//}
- (IBAction)showCrippleMap:(id)sender {
    self.leftButton.layer.borderWidth = 3;
    self.rightButton.layer.borderWidth = 0;

    self.isShowCripple = YES;
    NSError *error = nil;
    loadedLocations = self.loaderHelper.loadedLocations;
    if (loadedLocations.count < 2)
    {
        LocationInfo *currentLocation = [[LocationInfo alloc]init];
        currentLocation.location.name = @"Leningradskiy Railway Station 2";
        [self.loaderHelper startDownloadProcess:currentLocation :YES];
    } else {
        [self.loaderHelper selectLocation:loadedLocations[1] error:&error];
        [self loadVenues];
    }
}
- (IBAction)showOriginalMap:(id)sender {
    self.rightButton.layer.borderWidth = 3;
    self.leftButton.layer.borderWidth = 0;

    self.isShowCripple = NO;
    [self loadMap];
}

- (void)viewDidLoad{
  [super viewDidLoad];
    self.isShowCripple = NO;
    
    self.leftButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.rightButton.layer.borderColor = [UIColor whiteColor].CGColor;

    self.leftButton.layer.cornerRadius = self.leftButton.frame.size.height/2;
    self.rightButton.layer.cornerRadius = self.rightButton.frame.size.height/2;
    
    [SVProgressHUD showWithStatus:@"Загрузка"];
    
    self.contentScrollView.contentSize = CGSizeMake(self.contentScrollView.frame.size.width*2, self.contentScrollView.frame.size.height);
    self.showVenuesButton.enabled = NO;
    loadArchiveLock = [NSLock new];
    [self.navigineManager setUserHash:@"410B-04B0-8E98-A437"];
    
  // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"shevrone.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"Ленинградский вокзал" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"GothamPro-Light" size:13]];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 200, 32)];
    [backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    negativeSpacer.width = -25;
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer, backButtonItem] animated:NO];
    self.navigationItem.hidesBackButton = YES;

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateMap) name:@"setLocation" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(saveVenues) name:@"didRangeVenues" object:nil];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showRouteToVenue:) name:@"showRouteToVenue" object:nil];
    
    TTSwitch *switcher = [[TTSwitch alloc] initWithFrame:(CGRect){ CGPointZero, { 155.0f, 37.0f } }];
    switcher.layer.cornerRadius = switcher.frame.size.height/2.;
    switcher.layer.masksToBounds = YES;
    switcher.trackImage = [UIImage imageNamed:@"soundback"];
    switcher.thumbImage = [UIImage imageNamed:@"slider_knob_on"];
    switcher.maskInLockPosition = 0;
    [switcher setChangeHandler:^(BOOL on) {
        self.isAudioEnabled = on;
    }];
    switcher.center = CGPointMake(self.view.frame.size.width/2, self.leftButton.center.y);
    
    [self.contentScrollView addSubview:switcher];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activityIndicator.alpha = 1.0;
    self.activityIndicator.center = CGPointMake(self.contentScrollView.frame.size.width * 1.5, self.contentScrollView.frame.size.height/2);
    self.activityIndicator.hidesWhenStopped = YES;
    [self.contentScrollView addSubview:self.activityIndicator];
    [self.activityIndicator startAnimating];
    
    self.mapListSwitch.userInteractionEnabled = NO;
    self.mapListSwitch.hidden = YES;
    self.mapListSwitch = [[TTSwitch alloc]initWithFrame:(CGRect){ CGPointZero, { 70, 30 } }];
    self.mapListSwitch.layer.cornerRadius = self.mapListSwitch.frame.size.height/2.;
    self.mapListSwitch.layer.masksToBounds = YES;
    self.mapListSwitch.trackImage = [UIImage imageNamed:@"mapSwitchBack"];
    self.mapListSwitch.thumbImage = [UIImage imageNamed:@"mapThumb"];
    self.mapListSwitch.maskInLockPosition = 0;
    self.mapListSwitch.thumbOffsetY = 0;
    self.mapListSwitch.thumbInsetX = 0;
    [self.mapListSwitch setChangeHandler:^(BOOL on) {
        if (on)
        {
            self.mapListSwitch.thumbImage = [UIImage imageNamed:@"listThumb"];
            [self.contentScrollView setContentOffset:CGPointMake(self.contentScrollView.frame.size.width, 0) animated:YES];

        } else {
            self.mapListSwitch.thumbImage = [UIImage imageNamed:@"mapThumb"];
            [self.contentScrollView setContentOffset:CGPointMake(0, 0) animated:YES];


        }
    }];
    
    [self.mapListSwitch setOn:self.showList animated:YES];
    
    self.mapListSwitch.center = self.showVenuesButton.center;
    self.showVenuesButton.hidden = YES;
    
    [self.topBar addSubview:self.mapListSwitch];
  self.sv.backgroundColor = kColorFromHex(0xEAEAEA);
    
  processPath = [[UIBezierPath alloc] init];
  [processPath moveToPoint:CGPointMake(0, 11.f)];
  processLayer = [CAShapeLayer layer];
  processLayer.path            = [processPath CGPath];
  processLayer.strokeColor     = [kColorFromHex(0x4AADD4) CGColor];
  processLayer.lineWidth       = 22.f;
  processLayer.lineJoin        = kCALineJoinRound;
  processLayer.fillColor       = [[UIColor clearColor] CGColor];
  
  [self addRouteErrorViewWithTitle:@"It is impossible to build a route.      You are out of range of navigation"];
  
  self.navigineManager = [NavigineManager sharedManager];
  self.navigineManager.stepsDelegate = self;
    
    
    
    
  self.debugHelper = [DebugHelper sharedInstance];
  
  logsCount = 0;
  
  viewWithNoZoom = [[UIView alloc] init];
  
    if (!self.mapHelper) {
  self.mapHelper = [MapHelper sharedInstance];
  self.mapHelper.delegate = self;
  self.mapHelper.venueDelegate = nil;
    }
    self.loaderHelper = [LoaderHelper sharedInstance];
    self.loaderHelper.loaderDelegate = self;
    
    
    
  self.rotateButton.hidden = NO;
  self.rotateButton.alpha = 1.f;
  self.rotateButton.layer.cornerRadius = self.rotateButton.height/2.f;
  self.rotateButton.transform = CGAffineTransformMakeRotation(M_PI/4.);
  
  self.zoomInBtn.layer.cornerRadius = self.zoomInBtn.height/2.f;
  self.zoomOutBtn.layer.cornerRadius = self.zoomOutBtn.height/2.f;
  
  self.btnDownFloor.transform = CGAffineTransformMakeRotation(M_PI);
  self.btnDownFloor.hidden = NO;
  
  self.btnUpFloor.hidden = NO;
  
  lineWith = 2.f;
  
  enableFollow = NO;
  dynamicModeTimer = nil;
  centeredAroundCurrent = NO;
  error4count = 0;
  zoomScale = 1.0f;
  pins = [[NSMutableArray alloc] init];
  
  
  [self.sv addSubview:self.contentView];
  current = [[PositionOnMap alloc] init];
//    [current setImage:[UIImage imageNamed:@"userPosition"]];

  current.backgroundColor = [UIColor blackColor];
  
  current.hidden = YES;
  [self.contentView addSubview:current];
  
  errorViewType = ErrorViewTypeNone;
  
  UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
  longPress.minimumPressDuration = 1;
//  longPress.delaysTouchesBegan   = NO;
  [_sv addGestureRecognizer:longPress];
  
  UIRotationGestureRecognizer *rotate=[[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotation:)];
  [self.contentView addGestureRecognizer:rotate];
  
  UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
//  tapPress.delaysTouchesBegan   = NO;
  [_sv addGestureRecognizer:tapPress];
  
  UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scale:)];
  [pinchRecognizer setDelegate:self];
  [_sv addGestureRecognizer:pinchRecognizer];
  
  [self.progressBar.layer addSublayer:processLayer];
  self.automaticallyAdjustsScrollViewInsets = NO;
    
          CGAffineTransform rotationTransform = CGAffineTransformIdentity;
          rotationTransform = CGAffineTransformRotate(rotationTransform, -M_PI_2);
          _sv.transform = rotationTransform;
    
    _sv.frame = CGRectMake(0, 0, self.contentScrollView.frame.size.width, self.contentScrollView.frame.size.height);
    [self loadMap];
 

}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) rotation:(UIRotationGestureRecognizer *) sender{
  if (([sender state] == UIGestureRecognizerStateBegan ||
       [sender state] == UIGestureRecognizerStateChanged) &&
      !enableFollow) {
    [sender view].transform = CGAffineTransformRotate([[sender view] transform], [(UIRotationGestureRecognizer *)sender rotation]);
    //    [(UIRotationGestureRecognizer *)sender setRotation:0];
  }
}

- (void)setLocation:(LocationInfo *)locationInfo
{
    [self.navigineManager startNavigine];
    [self.navigineManager startRangePushes];
    [self.navigineManager startRangeVenues];
    
    [self updateMap];
    
}

-(void)loadMap
{
//    [SVProgressHUD showWithStatus:@"Загрузка карты"];
    loadedLocations = self.loaderHelper.loadedLocations;
    NSError *error = nil;

    if(loadedLocations.count){
        [self.loaderHelper selectLocation:loadedLocations[0] error:&error];
        [self loadVenues];

    } else {
        LocationInfo *currentLocation = [[LocationInfo alloc]init];
        currentLocation.location.name = @"Leningradskiy Railway Station";
        [self.loaderHelper startDownloadProcess:currentLocation :YES];
    }
    
}

-(void)successfullDownloading:(LocationInfo *)locationInfo{
    

    
        [locationInfo.circle removeFromSuperlayer];
        locationInfo.isDownloaded = YES;
        locationInfo.isDownloadingNow = NO;
        NSError *error = nil;
        [self.loaderHelper selectLocation:locationInfo error:&error];
        if(error != nil){
            locationInfo.isValidArchive = NO;
            locationInfo.isSet = NO;
        }
        else{
            locationInfo.isValidArchive = YES;
        }
        locationInfo.serverVersion = locationInfo.location.version;
//    [self.tableView reloadData];
}

-(void)updateMap
{
    [self viewWillAppear:YES];
}

-(void)saveVenues
{
    NSFileManager *fileManager = [NSFileManager defaultManager];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"venues.txt"];
    NSString *catsFile = [documentsDirectory stringByAppendingPathComponent:@"categories.txt"];
    
    NSError *error;
    [fileManager removeItemAtPath:appFile error:&error];
    [fileManager removeItemAtPath:catsFile error:&error];

    [NSKeyedArchiver archiveRootObject:[self.navigineManager venues] toFile:appFile];
    [NSKeyedArchiver archiveRootObject:[self.navigineManager categories] toFile:catsFile];
    
    [self loadVenues];
}

-(void)loadVenues
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"venues.txt"];
    NSString *catsFile = [documentsDirectory stringByAppendingPathComponent:@"categories.txt"];

    self.navigineManager.categories = [NSKeyedUnarchiver unarchiveObjectWithFile:catsFile];
    
    self.navigineManager.venues = [NSKeyedUnarchiver unarchiveObjectWithFile:appFile];
    
    if (self.navigineManager.venues.count > 0)
    {
        [self updateVenues];
    }

}

-(void)updateVenues
{
    [self viewWillAppear:YES];
    [self findUser];
    [SVProgressHUD dismiss];
    self.mapListSwitch.userInteractionEnabled = YES;
    self.mapListSwitch.hidden = NO;
    [self.activityIndicator stopAnimating];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Map" bundle:nil];
    RSVenueCategoriesViewController * vc = (RSVenueCategoriesViewController *)[sb instantiateViewControllerWithIdentifier:@"venuesVC"];
    vc.venuePoints = [self.navigineManager venues];
    vc.categories = [self.navigineManager categories];
    [self addChildViewController:vc];
    vc.view.frame = CGRectMake(self.contentScrollView.frame.size.width, 0, self.contentScrollView.frame.size.width, self.contentScrollView.frame.size.height);
    [self.contentScrollView addSubview:vc.view];
//    [content didMoveToParentViewController:self];
}

-(void)scale:(UIPinchGestureRecognizer*)gestureRecognizer{
  if ([gestureRecognizer numberOfTouches] < 2)
    return;
  
  float scale = gestureRecognizer.scale - 1;
  [gestureRecognizer setScale:1];
  [_sv setZoomScale:zoomScale + scale animated:NO];
  zoomScale = _sv.zoomScale;
  [self movePositionWithZoom:NO];
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
  if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
    //[gestureRecognizer setScale:1];
//    UIView *piece = gestureRecognizer.view;
//    CGPoint locationInView = [gestureRecognizer locationInView:piece];
//    CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
//    
//    piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
//    piece.center = locationInSuperview;
  }
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:
(UIGestureRecognizer *)otherGestureRecognizer{
  return YES;
}

  //
  //  CGPoint anchor = [recognizer locationInView:imageToScale];
  //  anchor = CGPointMake(anchor.x - imageToScale.bounds.size.width/2, anchor.y-imageToScale.bounds.size.height/2);
  //
  //  CGAffineTransform affineMatrix = imageToScale.transform;
  //  affineMatrix = CGAffineTransformTranslate(affineMatrix, anchor.x, anchor.y);
  //  affineMatrix = CGAffineTransformScale(affineMatrix, [recognizer scale], [recognizer scale]);
  //  affineMatrix = CGAffineTransformTranslate(affineMatrix, -anchor.x, -anchor.y);
  //  imageToScale.transform = affineMatrix;
  //
  //  [recognizer setScale:1];
//}

- (CGRect)zoomRectForScrollView:(UIScrollView *)scrollView withScale:(float)scale withCenter:(CGPoint)center {
  
  CGRect zoomRect;
  
  // The zoom rect is in the content view's coordinates.
  // At a zoom scale of 1.0, it would be the size of the
  // imageScrollView's bounds.
  // As the zoom scale decreases, so more content is visible,
  // the size of the rect grows.
  zoomRect.size.height = scrollView.frame.size.height / scale;
  zoomRect.size.width  = scrollView.frame.size.width  / scale;
  
  // choose an origin so as to get the right center.
  zoomRect.origin.x = center.x - (zoomRect.size.width  / 2.0);
  zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0);
  
  return zoomRect;
}


- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

}

- (void)showVenue{
    [self.mapListSwitch setOn:NO animated:YES];
    
    if([self.mapHelper.venueDelegate respondsToSelector:@selector(routeToPlace)]) {
        if([[self.mapHelper.venueDelegate showType] isEqualToString:@"route"]) {
            Venue *v = [self.mapHelper.venueDelegate routeToPlace];
            selectedVenue = v;
            CGFloat mapWidthInMeter = [self.navigineManager DEFAULT_WIDTH];
            
            CGFloat mapWidthInHeight = [self.navigineManager DEFAULT_HEIGHT];
            
            CGFloat xPoint =  v.kx.doubleValue * mapWidthInMeter;
            CGFloat yPoint =  v.ky.doubleValue * mapWidthInHeight;
            
            CGPoint point = CGPointMake(xPoint, yPoint);
        
            NSUInteger floor = [self.mapHelper.sublocId indexOfObject:[NSNumber numberWithInteger:v.sublocationId]];
            self.mapHelper.floor = floor;
            [self changeFloorTo:floor];
            [self startRouteWithFinishPoint:point andRouteType:RouteTypeFromIcon];
            [self findUser];
//            CGFloat mapOriginalWidth = (CGFloat)self.contentView.bounds.size.width;
//            CGFloat poX = [v.kx floatValue];
//            
//            CGFloat mapOriginalHeight = (CGFloat)self.contentView.bounds.size.height;
//            CGFloat poY = [v.ky floatValue];
//            
//            CGFloat xPoint1 =  (poX * mapOriginalWidth);
//            CGFloat yPoint1 =  mapOriginalHeight - poY * mapOriginalHeight;
//            
//            CGPoint point1 = CGPointMake(xPoint1, yPoint1);
//            
//            CGSize zoomSize;
//            zoomSize.width  = _sv.bounds.size.width;
//            zoomSize.height = _sv.bounds.size.height;
//            //offset the zoom rect so the actual zoom point is in the middle of the rectangle
//            
//            CGRect zoomRect;
//            zoomRect.origin.x    = (point1.x*zoomScale - zoomSize.width / 2.0f);
//            zoomRect.origin.y    = (point1.y*zoomScale - zoomSize.height / 2.0f);
//            zoomRect.size.width  = zoomSize.width;
//            zoomRect.size.height = zoomSize.height;
//            
//            _sv.contentOffset = CGPointMake(zoomRect.origin.x, zoomRect.origin.y);

        }
    }
    self.mapHelper.venueDelegate = nil;
    self.sv.pinchGestureRecognizer.enabled = NO;
}

- (void)selectPinWithVenue:(Venue *)v
{
  for (MapPin *m in pins) {
    if(m.venue == v) {
      currentPin = m;
        currentPin.mapView.hidden = YES;

        if (selectedVenue.sublocationId == res.outSubLocation)
        {
            currentPin.mapView.hidden = NO;

        }
//      [self zoomToPoint:currentPin.center withScale:1.0 animated:YES];
      [self showAnnotationForMapPin:currentPin];
    }
  }
}

- (void)didReceiveMemoryWarning{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}


- (void)movePositionWithZoom:(BOOL)isZoom {
    res = [self.navigineManager getNavigationResults];
    if (res.kX == 0) return;
  if (self.mapHelper.navigationType == NavigationTypeLog){
    logsCount++;
    if(logsCount >= logsTotalCount){
      res.ErrorCode = 4;
      [UIView animateWithDuration:0.5f animations:^{
        self.progressBar.bottom = -22.f;
      }];
    }
    [processPath addLineToPoint:CGPointMake(320.f * logsCount/logsTotalCount, 11.f)];
    processLayer.hidden = NO;
    processLayer.path            = [processPath CGPath];
  }
  if((res.X == 0.0 && res.Y == 0.0) || res.ErrorCode != 0)  {
    
    if(res.ErrorCode == 4 && error4count < 10) {
      error4count++;
    }
    else {
      current.hidden = YES;
      //      arrow.hidden = YES;
      routeLayer.hidden = YES;
      return;
    }
  }
  if(res.ErrorCode == 0){
    if(errorViewType == ErrorViewTypeNavigation){
      errorViewType = ErrorViewTypeNone;
      [routeErrorView removeFromSuperview];
      routeErrorView.hidden = YES;
      routeErrorView = nil;
      [errorViewTimer invalidate];
      errorViewTimer = nil;
    }
    current.hidden = NO;
  }
  
    CGFloat mapWidthInMeter = [self.navigineManager DEFAULT_WIDTH]; if (mapWidthInMeter == 0) return;
    CGFloat mapOriginalWidth = (CGFloat)self.contentView.bounds.size.width;
    CGFloat poX = (CGFloat)res.X;
    
    CGFloat mapWidthInHeight = [self.navigineManager DEFAULT_HEIGHT];
    CGFloat mapOriginalHeight = (CGFloat)self.contentView.bounds.size.height;
    CGFloat poY = (CGFloat)res.Y;
    
    CGFloat xPoint =  (poX * mapOriginalWidth) / mapWidthInMeter;
    CGFloat yPoint =  mapOriginalHeight - poY * mapOriginalHeight / mapWidthInHeight;
    
    CGPoint point = CGPointMake(xPoint, yPoint);
    CGFloat xPixInMeter = (CGFloat)mapOriginalWidth/mapWidthInMeter;
    CGFloat yPixInMeter = (CGFloat)mapOriginalWidth/mapWidthInMeter;
    CGRect pointFrame = CGRectMake(-xPixInMeter * res.R,-xPixInMeter * res.R, 2.0f * xPixInMeter * res.R, 2.0f * yPixInMeter * res.R);

        current.originalCenter = point;
        [UIView animateWithDuration:1.0/10 animations:^{
            current.background.frame = pointFrame;
//            current.transform= CGAffineTransformMakeRotation((CGFloat)res.Yaw);
            current.background.layer.cornerRadius = current.background.height/2.f;
            current.center = point;
        }];
  
  if (!res.ErrorCode && !centeredAroundCurrent){
    centeredAroundCurrent = YES;
    [self zoomToPoint:point withScale:1. animated:NO];
  }
  current.hidden = NO;
  //  arrow.hidden = NO;
  
    for (Vertex *vertex in self.cornerVertexes)
    {
        if ((vertex.x - res.X < 1)&&(vertex.y - res.Y < 1))
        {
            if (self.isAudioEnabled)
            {
                [[RSAudioHelperManager sharedManager]play:vertex.cornerType];
            }
        }
    }
    
  if (sublocationId != res.outSubLocation){
    current.hidden = YES;
    if(enableFollow){
      NSUInteger floor = [self.mapHelper.sublocId indexOfObject:[NSNumber numberWithInteger:res.outSubLocation]];
      self.btnUpFloor.alpha = 1.f;
      self.btnDownFloor.alpha = 1.f;
      if(floor == 0)
        self.btnDownFloor.alpha = 0.7f;
      if(floor == self.mapHelper.sublocId.count - 1)
        self.btnUpFloor.alpha = 0.7f;
      self.mapHelper.floor = floor;
      [self changeFloorTo:floor];
    }
  }
  else{
    current.hidden = NO;
    //    arrow.hidden = NO;
  }
  
  if(enableFollow ){
    CGSize zoomSize;
    zoomSize.width  = _sv.bounds.size.width;
    zoomSize.height = _sv.bounds.size.height;
    //offset the zoom rect so the actual zoom point is in the middle of the rectangle
    
    CGRect zoomRect;
    zoomRect.origin.x    = (point.x*zoomScale - zoomSize.width / 2.0f);
    zoomRect.origin.y    = (point.y*zoomScale - zoomSize.height / 2.0f);
    zoomRect.size.width  = zoomSize.width;
    zoomRect.size.height = zoomSize.height;
    
    _sv.contentOffset = CGPointMake(zoomRect.origin.x, zoomRect.origin.y);
  }
  
  
  if(isZoom) {
    [self zoomToPoint:point withScale:1 animated:YES];
  }
  
  CGPoint rPoint = CGPointMake(routePoint.x, routePoint.y);
    if(!CGPointEqualToPoint(rPoint, CGPointZero)) {
        NSArray *paths = [self.navigineManager routePaths];
        NSArray *distances = [self.navigineManager routeDistances];
        if (paths && paths.count && ((NSArray *)paths[0]).count){
            isRoutingNow = YES;
            NSArray *path = paths[0];
            NSNumber *distance = distances[0];
            
            if (!self.cornerVertexes)
            {
                self.oldPath = path;
                [self findCornersInPath:path];
            } else if (path != self.oldPath)
            {
                self.oldPath = path;
                [self findCornersInPath:path];
            }
            
            if (path.count) {
                [self drawRouteWithPath:path andDistance:distance];
            }
        }
        else{
//            errorViewType.type = ErrorViewTypeNoGraph;
        }
    }
//    else if (isRoutingToSuperVenue && _navigineManager.superVenue){
//        CGFloat mapWidthInMeter = [self.navigineManager DEFAULT_WIDTH];
//        CGFloat mapWidthInHeight = [self.navigineManager DEFAULT_HEIGHT];
//        CGFloat xPoint =  _navigineManager.superVenue.kx.doubleValue * mapWidthInMeter;
//        CGFloat yPoint =  _navigineManager.superVenue.ky.doubleValue * mapWidthInHeight;
//        CGPoint point = CGPointMake(xPoint, yPoint);
//        [self startRouteWithFinishPoint:point andRouteType:RouteTypeFromIcon];
//    }
  
}

-(void)findCornersInPath:(NSArray *)path
{
    self.cornerVertexes = [NSMutableArray new];
    
    for (int i = 0 ; i < path.count ; i++)
    {
        if (i < path.count - 2)
        {
            Vertex *first = path[i];
            Vertex *second = path[i+1];
            Vertex *third = path[i+2];
            
            if ((fabs(first.x - second.x) < 1)&&(fabs(first.x - third.x) > 1))
            {
                if (((first.x - third.x < 0)&&(first.y - third.y > 0))||((first.x - third.x > 0)&&(first.y - third.y < 0)))
                {
                    [second setCornerType:@"left"];
                    [self.cornerVertexes addObject:second];

                } else if (((first.x - third.x > 0)&&(first.y - third.y > 0))||((first.x - third.x < 0)&&(first.y - third.y < 0)))
                {
                    [second setCornerType:@"right"];
                    [self.cornerVertexes addObject:second];

                }
            } else if ((fabs(first.y - second.y) < 1)&&(fabs(first.y - third.y) > 1))
            {
                if (((first.x - third.x < 0)&&(first.y - third.y < 0))||((first.x - third.x > 0)&&(first.y - third.y > 0)))
                {
                    [second setCornerType:@"left"];
                    [self.cornerVertexes addObject:second];

                } else if (((first.x - third.x < 0)&&(first.y - third.y > 0))||((first.x - third.x > 0)&&(first.y - third.y < 0)))
                {
                    [second setCornerType:@"right"];
                    [self.cornerVertexes addObject:second];

                }
            }
                
        }
    }
}

CGAffineTransform CGAffineTransformMakeRotationAtPointWithZoom(CGFloat angle, CGPoint pt, CGFloat scale){
  const CGFloat fx = pt.x;
  const CGFloat fy = pt.y;
  const CGFloat fcos = cos(angle);
  const CGFloat fsin = sin(angle);
  return CGAffineTransformMake(fcos*scale, fsin*scale, -fsin*scale, fcos*scale, (fx - fx * fcos + fy * fsin)*scale, (fy - fx * fsin - fy * fcos)*scale);
}

CGAffineTransform CGAffineTransformMakeRotationAtPoint(CGFloat angle, CGPoint pt){
  const CGFloat fx = pt.x;
  const CGFloat fy = pt.y;
  const CGFloat fcos = cos(angle);
  const CGFloat fsin = sin(angle);
  return CGAffineTransformMake(fcos, fsin, -fsin, fcos, (fx - fx * fcos + fy * fsin), (fy - fx * fsin - fy * fcos));
}


-(void) drawRouteWithPath: (NSArray *)path
              andDistance: (NSNumber *) distance {
    [routeLayer removeFromSuperlayer];
    routeLayer = nil;
    [uipath removeAllPoints];
    uipath = nil;
    // We check that we are close to the finish point of the route
    if (distance.doubleValue <= 3.){
        [self stopRoute];
    }
    else{
        uipath     = [[UIBezierPath alloc] init];
        routeLayer = [CAShapeLayer layer];
        UIBezierPath *localPath = nil;
        for (int i = 0; i < path.count; i++ ){
            Vertex *vertex = path[i];
            if(vertex.subLocation != sublocationId){
                [uipath appendPath:localPath];
                localPath = nil;
                continue;
            }
            CGFloat mapWidthInMeter = [self.navigineManager DEFAULT_WIDTH];
            CGFloat mapOriginalWidth = (CGFloat)self.contentView.bounds.size.width;
            CGFloat poX = (CGFloat)vertex.x;
            CGFloat mapWidthInHeight = [self.navigineManager DEFAULT_HEIGHT];
            CGFloat mapOriginalHeight = (CGFloat)self.contentView.bounds.size.height;
            CGFloat poY = (CGFloat)vertex.y;
            CGFloat xPoint =  (poX * mapOriginalWidth) / mapWidthInMeter;
            CGFloat yPoint =  mapOriginalHeight - poY * mapOriginalHeight / mapWidthInHeight;
            if(!localPath) {
                localPath = [UIBezierPath bezierPath];
                [uipath moveToPoint:CGPointMake(xPoint, yPoint)];
            }
            else {
                [uipath addLineToPoint:CGPointMake(xPoint, yPoint)];
            }
        }
    }
    routeLayer.hidden = NO;
    routeLayer.path            = [uipath CGPath];
    if (!self.isShowCripple)
    {
    routeLayer.strokeColor     = [kColorFromHex(0x4AADD4) CGColor];
    } else {
        routeLayer.strokeColor     = [[UIColor colorWithRed:0.46 green:0.84 blue:0.19 alpha:1.00] CGColor];

    }
    routeLayer.lineWidth       = lineWith;
    routeLayer.lineJoin        = kCALineJoinRound;
    routeLayer.fillColor       = [[UIColor clearColor] CGColor];
    [self.contentView.layer addSublayer:routeLayer];
    [self.contentView bringSubviewToFront:current];
}



- (void)addPinToMapWithVenue:(Venue *)v andImage:(UIImage *)image{
  CGFloat mapWidthInMeter = [self.navigineManager DEFAULT_WIDTH];
  CGFloat mapOriginalWidth = (CGFloat)self.contentView.bounds.size.width;
  
  CGFloat mapWidthInHeight = [self.navigineManager DEFAULT_HEIGHT];
  CGFloat mapOriginalHeight = (CGFloat)self.contentView.bounds.size.height;
  
  CGFloat xPoint =  v.kx.doubleValue * mapOriginalWidth;
  CGFloat yPoint =  mapOriginalHeight - v.ky.doubleValue * mapOriginalHeight;
  
  CGPoint point = CGPointMake(xPoint, yPoint);
  
  MapPin *btnPin = [[MapPin alloc] initWithVenue:v];
  [btnPin setImage:image forState:UIControlStateNormal];
  [btnPin setImage:image forState:UIControlStateHighlighted];
  [btnPin addTarget:self action:@selector(btnPinPressed:) forControlEvents:UIControlEventTouchUpInside];
  [btnPin sizeToFit];
    btnPin.venue = v;
  btnPin.center  = point;
    
    
    CGAffineTransform rotationTransform = CGAffineTransformIdentity;
    rotationTransform = CGAffineTransformRotate(rotationTransform, M_PI_2);
    btnPin.transform = rotationTransform;
    
//    _sv.frame = self.contentScrollView.bounds;
    
  [viewWithNoZoom addSubview:btnPin];
  btnPin.originalCenter = btnPin.center;
  [pins addObject:btnPin];
}

- (void)btnPinPressed:(id)sender {
  currentPin = (MapPin *)sender;
  if(!currentPin.mapView.hidden){
    [currentPin.mapView removeFromSuperview];
    currentPin.mapView.hidden = YES;
  }
  else{
    for(MapPin *mapPin in pins){
      [mapPin.mapView removeFromSuperview];
      mapPin.mapView.hidden = YES;
    }
    
    currentPin.mapView.hidden = NO;
    if(!enableFollow)
      [self zoomToPoint:currentPin.originalCenter withScale:1.0 animated:YES];
    [self showAnnotationForMapPin:currentPin];
  }
}

- (void)showAnnotationForMapPin:(MapPin *)mappin {
//    [self deselectPins];
  [viewWithNoZoom addSubview:mappin.mapView];

    CGAffineTransform rotationTransform = CGAffineTransformIdentity;
    rotationTransform = CGAffineTransformRotate(rotationTransform, M_PI_2);
    mappin.mapView.transform = rotationTransform;
    
    selectedVenue = mappin.venue;
    [mappin.btnVenue addTarget:self action:@selector(btnVenuePressed:) forControlEvents:UIControlEventTouchUpInside];
    
  mappin.mapView.left = 0.f;
  mappin.mapView.centerX  = mappin.centerX + 70;
  mappin.mapView.alpha = 0.f;
  //Animate drop
  [UIView animateWithDuration:0.2 delay:0 options: UIViewAnimationOptionCurveLinear animations:^{
    mappin.mapView.center = CGPointMake(mappin.center.x + 35, mappin.center.y);
    mappin.mapView.alpha = 1.f;
  } completion:^(BOOL finished){
  }];
//  mappin.mapView.bottom   = mappin.top - 9.0f;
    

  
}

- (void)btnVenuePressed:(id)sender
{
    Venue *v = selectedVenue;
    
    CGFloat mapWidthInMeter = [self.navigineManager DEFAULT_WIDTH];
    
    CGFloat mapWidthInHeight = [self.navigineManager DEFAULT_HEIGHT];
    
    CGFloat xPoint =  v.kx.doubleValue * mapWidthInMeter;
    CGFloat yPoint =  v.ky.doubleValue * mapWidthInHeight;
    
    [self startRouteWithFinishPoint:CGPointMake(xPoint, yPoint) andRouteType:RouteTypeFromIcon];
    
    selectedVenue = nil;
//  [self performSegueWithIdentifier:@"placeSegue" sender:self];
//  [self deselectPins];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
  if ([segue.identifier hasPrefix:@"placeSegue"]) {
    PlaceView *pw = (PlaceView *)segue.destinationViewController;
    pw.venues = currentPin.venue;
    pw.navigationController.navigationBarHidden = YES;
  } else if ([segue.identifier isEqualToString:@"showVenueList"])
  {
      RSVenueCategoriesViewController *venuePointsVC = segue.destinationViewController;
      venuePointsVC.venuePoints = [self.navigineManager venues];
      venuePointsVC.categories = [self.navigineManager categories];
  }
}

- (IBAction)backRoutePressed:(id)sender {
  [self.navigationController setNavigationBarHidden:NO animated:YES];
  
  _routeType = RouteTypeNone;
  self.sv.origin = CGPointZero;
  [self stopRoute];
}

- (void)deselectPins {
  if(pin && !isRoutingNow){
    [pin removeFromSuperview];
    [pin.unnotationView removeFromSuperview];
    pin = nil;
  }
  if(isRoutingNow){
    [pin.unnotationView removeFromSuperview];
  }
  for(MapPin *mapPin in pins){
    [mapPin.mapView removeFromSuperview];
    mapPin.mapView.hidden = YES;
  }
}

- (void)tapPress:(UITapGestureRecognizer *)gesture {

    [self deselectPins];
}

- (void)startRouteWithFinishPoint:(CGPoint)point andRouteType:(RouteType)type {
  if(![self.navigineManager isNavigineFine]) {
    [self addRouteErrorViewWithTitle:@"It is impossible to build a route.      You are out of range of navigation"];
    errorViewType = ErrorViewTypeNavigation;
//    routeErrorView.hidden = NO;
    errorViewTimer = [NSTimer scheduledTimerWithTimeInterval:5.f
                                     target:self
                                   selector:@selector(dismissRouteErrorView:)
                                   userInfo:nil
                                    repeats:NO];
    
    if(pin && type == RouteTypeFromClick) {
      [pin removeFromSuperview];
      [pin.unnotationView removeFromSuperview];
    }
    return;
  }
  
  _routeType = type;
  
  if(isRoutingNow) {
    [self stopRoute];
  }
  
  routePoint.x = point.x;
  routePoint.y = point.y;
  routePoint.sublocationId = sublocationId;
  
  isRoutingNow = YES;
  [self.navigineManager addTatget:sublocationId :point.x :point.y];
}

- (void) dismissRouteErrorView :(NSTimer *)timer{
  routeErrorView.hidden = YES;
  [timer invalidate];
  errorViewType = ErrorViewTypeNone;
}

- (void) dynamicModeTimerInvalidate :(NSTimer *)timer{
  [dynamicModeTimer invalidate];
  dynamicModeTimer = nil;
  enableFollow = YES;
}

- (void)stopRoute {
  if(pin && (_routeType != RouteTypeFromClick || _routeType == RouteTypeNone)) {
    [pin removeFromSuperview];
    [pin.unnotationView removeFromSuperview];
  }
  if(errorViewType == ErrorViewTypeNewRoute){
    errorViewType = ErrorViewTypeNone;
    [routeErrorView removeFromSuperview];
    routeErrorView.hidden = YES;
    routeErrorView = nil;
    [errorViewTimer invalidate];
    errorViewTimer = nil;
 
  }
  
  isRoutingNow = NO;
  routePoint.x = 0;
  routePoint.y = 0;
  routePoint.sublocationId = -1;
  
  [routeLayer removeFromSuperlayer];
  routeLayer = nil;
  
  [uipath removeAllPoints];
  uipath = nil;
  [self.navigineManager cancelTargets];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
  return self.contentView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
        if(dynamicModeTimer){
            [dynamicModeTimer invalidate];
            dynamicModeTimer = nil;
        }
        enableFollow = NO;
        dynamicModeTimer = [NSTimer scheduledTimerWithTimeInterval:7.0
                                                            target:self
                                                          selector:@selector(dynamicModeTimerInvalidate:)
                                                          userInfo:nil
                                                           repeats:NO];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{

}

- (void) scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view{
  [pin resizePressPinWithZoom:scrollView.zoomScale];
  for(MapPin *mapPin in pins){
    [mapPin resizeMapPinWithZoom:scrollView.zoomScale];
  }
  [current resizePositionOnMapWithZoom:scrollView.zoomScale];
  if(enableFollow == YES){
    self.contentView.origin = CGPointMake(0.f, 0.f);
  }
  viewWithNoZoom.frame = self.contentView.frame;
  //  originOffset = self.sv.contentOffset;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
  [pin resizePressPinWithZoom:scrollView.zoomScale];
  for(MapPin *mapPin in pins){
    [mapPin resizeMapPinWithZoom:scrollView.zoomScale];
  }
  
  [current resizePositionOnMapWithZoom:scrollView.zoomScale];
  if(enableFollow == YES){
    self.contentView.origin = CGPointMake(0.f, 0.f);
    [self zoomRectForScrollView:scrollView withScale:scrollView.zoomScale withCenter:CGPointMake(0, 0)];
  }
  lineWith = 2.f / scrollView.zoomScale;
  if (self.sv.zoomScale < 1 && !enableFollow){
    if ( self.contentView.frame.size.height / self.contentView.frame.size.width > self.sv.frame.size.height / self.sv.frame.size.width){
      self.contentView.centerX = self.sv.width / 2.f;
    }
    else{
      self.contentView.centerY = self.sv.height / 2.f;
    }
  }
  viewWithNoZoom.frame = self.contentView.frame;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
  viewWithNoZoom.frame = self.contentView.frame;
  [self movePositionWithZoom:NO];

}

- (IBAction)currentLocationPressed:(id)sender {
  [self movePositionWithZoom:YES];
}



- (IBAction)zoomButton:(id)sender {
  UIButton *btn = (UIButton *)sender;
  
  [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
    btn.transform = CGAffineTransformMakeScale(1.2, 1.2);
  } completion:^(BOOL finished) {
    
  }];
}

-(void) addRouteErrorViewWithTitle: (NSString *)title{
  [routeErrorView removeFromSuperview];
  routeErrorView.hidden = YES;
  routeErrorView = nil;
  [errorViewTimer invalidate];
  errorViewTimer = nil;
  
  routeErrorView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, 87)];
  routeErrorView.backgroundColor = kColorFromHex(0xD36666);
//  routeErrorView.alpha = 0.9f;
  
  NSString *labelText = title;
  NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
  NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
  paragraphStyle.lineSpacing = 9.f;
  [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, 40)];
  
  UILabel *unavaliableRoute = [[UILabel alloc] initWithFrame:CGRectMake(33.f, 22.f, self.view.frame.size.width-66, 87-44)];
  unavaliableRoute.attributedText = attributedString;
  unavaliableRoute.textColor = kColorFromHex(0xFAFAFA);
  unavaliableRoute.font  = [UIFont fontWithName:@"Circe-Bold" size:13.0f];
  unavaliableRoute.textAlignment = NSTextAlignmentCenter;
  unavaliableRoute.numberOfLines = 0;
  [routeErrorView addSubview:unavaliableRoute];
  UITapGestureRecognizer *tapPressOnErrorView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
//  tapPressOnErrorView.delaysTouchesBegan   = NO;
  [routeErrorView addGestureRecognizer:tapPressOnErrorView];
  [self.view addSubview:routeErrorView];
    [self.view bringSubviewToFront:routeErrorView];
  routeErrorView.hidden = YES;
}

- (IBAction)zoomButtonOut:(id)sender {
  UIButton *btn = (UIButton *)sender;
  
  [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
    btn.transform = CGAffineTransformMakeScale(1.0, 1.0);
  } completion:^(BOOL finished) {
    
  }];
  
  if(sender == _zoomInBtn) {
    [_sv setZoomScale:_sv.zoomScale + 0.2f animated:YES];
  }
  else {
    [_sv setZoomScale:_sv.zoomScale - 0.2f animated:YES];
  }
}

- (void)zoomToPoint:(CGPoint)zoomPoint withScale:(CGFloat)scale animated: (BOOL)animated{
  //Normalize current content size back to content scale of 1.0f
  CGSize contentSize;
  contentSize.width  = (_sv.contentSize.width / _sv.zoomScale);
  contentSize.height = (_sv.contentSize.height / _sv.zoomScale);
  
  //translate the zoom point to relative to the content rect
  //  zoomPoint.x = (zoomPoint.x / _sv.bounds.size.width) * contentSize.width;
  //  zoomPoint.y = (zoomPoint.y / _sv.bounds.size.height) * contentSize.height;
  
  //derive the size of the region to zoom to
  CGSize zoomSize;
  zoomSize.width  = _sv.bounds.size.width / scale;
  zoomSize.height = _sv.bounds.size.height / scale;
  
  //offset the zoom rect so the actual zoom point is in the middle of the rectangle
  CGRect zoomRect;
  zoomRect.origin.x    = zoomPoint.x - zoomSize.width / 2.0f;
  zoomRect.origin.y    = zoomPoint.y - zoomSize.height / 2.0f;
  zoomRect.size.width  = zoomSize.width;
  zoomRect.size.height = zoomSize.height;
  
  //apply the resize
  
  //  zoomSize.width  = _sv.bounds.size.width;
  //  zoomSize.height = _sv.bounds.size.height;
  //  //offset the zoom rect so the actual zoom point is in the middle of the rectangle
  //
  //  zoomRect.origin.x    = (zoomPoint.x*zoomScale - self.view.width / 2.0f);
  //  zoomRect.origin.y    = (zoomPoint.y*zoomScale - self.view.height / 2.0f);
  //  zoomRect.size.width  = zoomSize.width;
  //  zoomRect.size.height = zoomSize.height;
  
  //  _sv.contentOffset = CGPointMake(zoomRect.origin.x, zoomRect.origin.y);
  
  [_sv zoomToRect:zoomRect animated: YES];
  
}

- (void)zoomToPoint:(CGPoint)zoomPoint animated: (BOOL)animated{
  //Normalize current content size back to content scale of 1.0f
  CGSize contentSize;
  contentSize.width  = (_sv.contentSize.width / _sv.zoomScale);
  contentSize.height = (_sv.contentSize.height / _sv.zoomScale);
  
  
  CGFloat mapWidthInMeter = [self.navigineManager DEFAULT_WIDTH];
  CGFloat mapOriginalWidth = (CGFloat)self.contentView.bounds.size.width;
  CGFloat poX = (CGFloat)res.X;
  
  CGFloat mapWidthInHeight = [self.navigineManager DEFAULT_HEIGHT];
  CGFloat mapOriginalHeight = (CGFloat)self.contentView.bounds.size.height;
  CGFloat poY = (CGFloat)res.Y;
  
  CGFloat xPoint =  (poX * mapOriginalWidth) / mapWidthInMeter;
  CGFloat yPoint =  mapOriginalHeight - poY * mapOriginalHeight / mapWidthInHeight;
  //translate the zoom point to relative to the content rect
  //  zoomPoint.x = (zoomPoint.x / _sv.bounds.size.width) * contentSize.width;
  //  zoomPoint.y = (zoomPoint.y / _sv.bounds.size.height) * contentSize.height;
  
  //derive the size of the region to zoom to
  CGSize zoomSize;
  zoomSize.width  = _sv.bounds.size.width;
  zoomSize.height = _sv.bounds.size.height;
  
  //offset the zoom rect so the actual zoom point is in the middle of the rectangle
  CGRect zoomRect;
  zoomRect.origin.x    = xPoint - zoomSize.width / 2.0f;
  zoomRect.origin.y    = yPoint - zoomSize.height / 2.0f;
  zoomRect.size.width  = zoomSize.width;
  zoomRect.size.height = zoomSize.height;
  
  //apply the resize
  
  [_sv zoomToRect:zoomRect animated: YES];
  [self.sv setZoomScale:1 animated:YES];
  viewWithNoZoom.frame = self.contentView.frame;
}

- (void)centerScrollViewContents {
  CGSize boundsSize = _sv.bounds.size;
  CGRect contentsFrame = self.contentView.frame;
  
  if (contentsFrame.size.width < boundsSize.width) {
    contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
  } else {
    contentsFrame.origin.x = 0.0f;
  }
  
  if (contentsFrame.size.height < boundsSize.height) {
    contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
  } else {
    contentsFrame.origin.y = 0.0f;
  }
  self.contentView.frame = contentsFrame;
  res = [self.navigineManager getNavigationResults];
  if(!res.ErrorCode){
    CGFloat mapWidthInMeter = [self.navigineManager DEFAULT_WIDTH];
    CGFloat mapOriginalWidth = (CGFloat)self.contentView.bounds.size.width;
    CGFloat poX = (CGFloat)res.X;
    
    CGFloat mapWidthInHeight = [self.navigineManager DEFAULT_HEIGHT];
    CGFloat mapOriginalHeight = (CGFloat)self.contentView.bounds.size.height;
    CGFloat poY = (CGFloat)res.Y;
    
    CGFloat xPoint =  (poX * mapOriginalWidth) / mapWidthInMeter;
    CGFloat yPoint =  mapOriginalHeight - poY * mapOriginalHeight / mapWidthInHeight;
    
    CGPoint point = CGPointMake(xPoint, yPoint);
    [self zoomToPoint:point animated:YES];
  }
  viewWithNoZoom.frame = self.contentView.frame;
}

- (void)longPress:(UIGestureRecognizer *)gesture {
  if (gesture.state == UIGestureRecognizerStateBegan) {
    translatedPoint = [(UIGestureRecognizer*)gesture locationInView:self.contentView];
    if(isRoutingNow){
        [self deselectPins];
    }
    
    if(pin) {
      [pin removeFromSuperview];
      [pin.unnotationView removeFromSuperview];
      pin = nil;
    }
    
    pin = [[PressPin alloc] initWithFrame:CGRectZero];
    [pin addTarget:self action:@selector(btnRoutePin:) forControlEvents:UIControlEventTouchUpInside];
    [pin sizeToFit];
    pin.center = CGPointMake(translatedPoint.x, 0);
    
    pin.bottom = translatedPoint.y;
    pin.centerX = translatedPoint.x;
    pin.hidden = NO;
    [viewWithNoZoom addSubview:pin];
    
    [pin.btn addTarget:self action:@selector(btnRoute:) forControlEvents:UIControlEventTouchUpInside];
    
    pin.unnotationView.bottom   = pin.top - 10;
    pin.unnotationView.centerX  = pin.centerX;
    pin.originalBottom = pin.bottom;
    pin.originalCenterX = pin.centerX;
    pin.sublocationId = sublocationId;
    [viewWithNoZoom addSubview:pin.unnotationView];
    [pin resizePressPinWithZoom:self.sv.zoomScale];
  }
}

- (IBAction) btnRoute:(id)sender{
  UIButton *btn = (UIButton *)sender;
  UIImageView *pipka = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"elmBubbleArrowBlue"]];
  
  CGPoint point = CGPointMake(pin.originalCenterX, pin.originalBottom);
  
  CGFloat mapWidthInMeter  = [self.navigineManager DEFAULT_WIDTH];
  CGFloat mapOriginalWidth = (CGFloat)self.contentView.bounds.size.width;
  
  CGFloat mapWidthInHeight  = [self.navigineManager DEFAULT_HEIGHT];
  CGFloat mapOriginalHeight = (CGFloat)self.contentView.bounds.size.height;
  
  CGFloat xPoint = (point.x / mapOriginalWidth) * mapWidthInMeter;
  CGFloat yPoint = (mapOriginalHeight - point.y) /  mapOriginalHeight * mapWidthInHeight;
  point = CGPointMake(xPoint , yPoint);
  
  [self startRouteWithFinishPoint:point andRouteType:RouteTypeFromClick];
  [pin.unnotationView removeFromSuperview];
}

- (IBAction)btnRoutePin:(id)sender{
  if(!isRoutingNow) return;
  [pin swithPinMode];
  [pin.btn addTarget:self action:@selector(btnCancelRoute:) forControlEvents:UIControlEventTouchUpInside];
  [viewWithNoZoom addSubview:pin.unnotationView];
}

-(IBAction)btnCancelRoute:(id)sender{
  [self stopRoute];
  if(pin){
    [pin removeFromSuperview];
    [pin.unnotationView removeFromSuperview];
  }
}

-(void)viewWillDisappear:(BOOL)animated{
  [super viewWillDisappear:animated];
  self.mapHelper.navigationType = NavigationTypeRegular;
  [current removeFromSuperview];
    [SVProgressHUD dismiss];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
    if (self.navigineManager.location.subLocations.count > 0)
    {
        if (!self.mapHelper)
        {
    self.mapHelper = [MapHelper sharedInstance];
        }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setLocation1" object:nil userInfo:@{@"locationName":@"Office"}];
//        [SVProgressHUD dismiss];
  self.mapHelper.delegate = self;
  if (self.mapHelper.sublocId.count == 1){
    self.btnDownFloor.hidden = YES;
    self.btnUpFloor.hidden = YES;
    self.txtFloor.hidden = YES;
  }
  else{
    self.btnDownFloor.hidden = NO;
    self.btnUpFloor.hidden = NO;
    self.txtFloor.hidden = NO;
    self.btnDownFloor.alpha = 0.7f;
  }
  if(self.mapHelper.navigationType == NavigationTypeRegular){
    self.progressBar.hidden = YES;
    UIImage *buttonImage = [UIImage imageNamed:@"btnMenu"];
    [self.leftButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
  }
  else{
    UIImage *buttonImage = [UIImage imageNamed:@"btnBack"];
    [self.leftButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    NSError *eror = nil;
    logsTotalCount = [self.navigineManager startNavigateByLog:self.debugHelper.navigateLogfile with:&eror];
    if (!eror)
      self.progressBar.hidden = NO;
  }
  _sv.maximumZoomScale = 5.0f;
  _sv.zoomScale = 1.0f;
  zoomScale = 1.0f;
    if (self.mapHelper.sublocId.count > 0)
    {
//        if (selectedVenue)
//        {
//            NSUInteger floor = [self.mapHelper.sublocId indexOfObject:[NSNumber numberWithInteger:selectedVenue.sublocationId]];
//            self.mapHelper.floor = floor;
//        }
  [self changeFloorTo:self.mapHelper.floor];
        selectedVenue = nil;
    }
    }
}
//
//- (IBAction)menuPressed:(id)sender {
//  if(self.mapHelper.navigationType == NavigationTypeRegular){
//    if(self.slidingPanelController.sideDisplayed == MSSPSideDisplayedLeft) {
//      [self.slidingPanelController closePanel];
//    }
//    else {
//      [self.slidingPanelController openLeftPanel];
//    }
//  }
//  else{
//    [self.navigineManager stopNavigeteByLog];
//    [self.navigationController popViewControllerAnimated:YES];
//  }
//}

- (IBAction)zoomInTouch:(id)sender {
  [_sv setZoomScale:zoomScale + 0.2f animated:NO];
  zoomScale = _sv.zoomScale;
  [self movePositionWithZoom:NO];
}

- (IBAction)zoomOutTouch:(id)sender {
  [_sv setZoomScale:zoomScale - 0.2f animated:NO];
  zoomScale = _sv.zoomScale;
  [self movePositionWithZoom:NO];
}

- (IBAction)upFloor:(id)sender {
  self.btnDownFloor.alpha = 1.f;
  
  if(self.mapHelper.floor != self.mapHelper.sublocId.count - 1){
    self.mapHelper.floor++;
    [self changeFloorTo:self.mapHelper.floor];
  }
  
  if(self.mapHelper.floor == self.mapHelper.sublocId.count - 1){
    self.btnUpFloor.alpha = 0.7f;
  }
}

- (IBAction)downFloor:(id)sender {
  if(self.mapHelper.floor == 0){
    return;
  }
  self.mapHelper.floor--;
  self.btnUpFloor.alpha = 1.f;
  [self changeFloorTo:self.mapHelper.floor];
  if(self.mapHelper.floor == 0){
    self.btnDownFloor.alpha = 0.7f;
  }
}

- (void) changeFloorTo:(NSInteger)row{
  if(self.mapHelper.floor == 0){
    self.btnDownFloor.alpha = 0.7f;
  }
    [self deselectPins];
  NSError *error = nil;
  sublocationId = [self.mapHelper.sublocId[row] intValue];
  [_sv setZoomScale:1.00001 animated:YES];
  CGSize imageSize = [self.navigineManager sizeForImageAtIndex:self.mapHelper.floor error:&error];
  
  if(error){
    [UIAlertView showWithTitle:@"ERROR" message:@"Incorrect width and height" cancelButtonTitle:@"OK"];
  }
  [self.contentView removeFromSuperview];
  self.contentView = self.mapHelper.webViewArray[row];
  viewWithNoZoom.frame = self.contentView.frame;
  
  for(UIImageView *p in pins) [p removeFromSuperview];
  if(pin){
    [pin removeFromSuperview];
    [pin.unnotationView removeFromSuperview];
  }
  
  [self.contentView addSubview:current];
  CGFloat minScale = 1.f;

  if ( self.contentView.frame.size.height / self.contentView.frame.size.width > self.sv.frame.size.height / self.sv.frame.size.width){
    minScale = self.sv.frame.size.height / self.contentView.frame.size.height;
  }
  else{
    minScale = self.sv.frame.size.width / self.contentView.frame.size.width;
  }
  
  _sv.minimumZoomScale = minScale;
  self.contentView.origin = CGPointZero;
//  self.sv.contentOffset = CGPointZero;
  
  for (Venue *v in [self.navigineManager venues]) {
    if(v.sublocationId == sublocationId){
      [self addPinToMapWithVenue:v  andImage:[self imageForCategory:v]];
//      [self.navigineManager addTatget:sublocationId :v.kx.doubleValue * self.navigineManager.DEFAULT_WIDTH :v.ky.doubleValue* self.navigineManager.DEFAULT_HEIGHT];
    }
  }
  if(pin.sublocationId == sublocationId){
    [viewWithNoZoom addSubview:pin];
    [viewWithNoZoom addSubview:pin.unnotationView];
  }
  [self.sv addSubview:self.contentView];
  self.contentView.hidden = NO;
  [self.sv addSubview:viewWithNoZoom];
  if(enableFollow){
    if (sublocationId != res.outSubLocation){
      if(dynamicModeTimer){
        dynamicModeTimer = nil;
      }
      enableFollow = NO;
      dynamicModeTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                          target:self
                                                        selector:@selector(dynamicModeTimerInvalidate:)
                                                        userInfo:nil
                                                         repeats:NO];
    }
  }
  
    if (selectedVenue)
    {
        [self selectPinWithVenue:selectedVenue];
//        selectedVenue = nil;
    }

    
  self.txtFloor.text = [NSString stringWithFormat:@"%zd", self.mapHelper.floor];
  [_sv setZoomScale:1.00001 animated:NO];
  zoomScale = _sv.zoomScale;
  [self movePositionWithZoom:NO];
//  [self centerScrollViewContents];
}

- (IBAction)findUserButtonTapped:(id)sender {
    [self findUser];
}

-(UIImage *)imageForCategory:(Venue *)venue
{
    switch (venue.category) {
        case 0:
//            [self.iconImageView setImage:[UIImage imageNamed:@"userLocationIcon"]];
            break;
            
        case 1:
            //            [self.nameLabel setText:@"Кафе и рестораны"];
            return [UIImage imageNamed:@"cofe"];
            break;
        case 2:
            //            [self.nameLabel setText:@"Напитки и сладости"];
            return [UIImage imageNamed:@"food"];
            break;
        case 3:
            //            [self.nameLabel setText:@"Одежда и мода"];
            return [UIImage imageNamed:@"shop"];
            break;
        case 4:
            //            [self.nameLabel setText:@"Парфюмерия"];
            return [UIImage imageNamed:@"shop"];
            break;
        case 5:
            //            [self.nameLabel setText:@"Драгоценности"];
            return [UIImage imageNamed:@"shop"];
            break;
        case 6:
            //            [self.nameLabel setText:@"Сувениры и подарки"];
            return [UIImage imageNamed:@"shop"];
            break;
        case 7:
            //            [self.nameLabel setText:@"Электроника"];
            return [UIImage imageNamed:@"shop"];
            break;
        case 8:
            //            [self.nameLabel setText:@"Товары в дорогу"];
            return [UIImage imageNamed:@"shop"];
            break;
        case 9:
            //            [self.nameLabel setText:@"Аптеки"];
            return [UIImage imageNamed:@"medic"];
            break;
        case 10:
            //            [self.nameLabel setText:@"Туалеты"];
            return [UIImage imageNamed:@"toilets"];
            break;
        case 11:
            //            [self.nameLabel setText:@"Камеры хранения"];
            return [UIImage imageNamed:@"save"];
            break;
        case 12:
            //            [self.nameLabel setText:@"Кассы"];
            return [UIImage imageNamed:@"tickets"];
            break;
        case 13:
            //            [self.nameLabel setText:@"Залы ожидания"];
            return [UIImage imageNamed:@"waiting"];
            break;
        case 14:
            //            [self.nameLabel setText:@"Банкомат"];
            return [UIImage imageNamed:@"money"];
            break;
        case 15:
            //            [self.nameLabel setText:@"Магазины"];
            return [UIImage imageNamed:@"shop"];
            break;
        case 16:
            //            [self.nameLabel setText:@"Военный комендант"];
            return [UIImage imageNamed:@"police"];
            break;
        case 17:
            //            [self.nameLabel setText:@"Рестораны"];
            return [UIImage imageNamed:@"food"];
            break;
        case 18:
            //            [self.nameLabel setText:@"Сервис"];
            return [UIImage imageNamed:@"servces"];
            break;
        case 19:
            //            [self.nameLabel setText:@"Интернет"];
            return [UIImage imageNamed:@"internet"];
            break;
        case 20:
            //            [self.nameLabel setText:@"сапсан"];
            return [UIImage imageNamed:@"sapsan"];
            break;
        default:
            break;
    }
    return [UIImage imageNamed:@"elmVenueIcon"];

}

-(void)findUser
{
    if (!self.showList)
    {
        [self.mapListSwitch setOn:NO animated:YES];
    }
    
    if(res.ErrorCode){
        [self addRouteErrorViewWithTitle:@"I can't detect your position.              You are out of range of navigation"];
        errorViewType = ErrorViewTypeNavigation;
//        routeErrorView.hidden = NO;
        errorViewTimer = [NSTimer scheduledTimerWithTimeInterval:5.f
                                                          target:self
                                                        selector:@selector(dismissRouteErrorView:)
                                                        userInfo:nil
                                                         repeats:NO];
    }
    else{
        if(errorViewType == ErrorViewTypeNavigation){
            routeErrorView.hidden = YES;
            routeErrorView = nil;
        }
        [_rotateButton setImage:[UIImage imageNamed:@"btnDynamicMap"] forState:UIControlStateNormal];
        self.rotateButton.transform = CGAffineTransformMakeRotation(0.f);
        enableFollow = YES;
        zoomScale = _sv.zoomScale;
        if (sublocationId != res.outSubLocation){
            NSUInteger floor = [self.mapHelper.sublocId indexOfObject:[NSNumber numberWithInteger:res.outSubLocation]];
            self.btnUpFloor.alpha = 1.f;
            self.btnDownFloor.alpha = 1.f;
            if(floor == 0)
                self.btnDownFloor.alpha = 0.7f;
            if(floor == self.mapHelper.sublocId.count - 1)
                self.btnUpFloor.alpha = 0.7f;
            self.mapHelper.floor = floor;
            [self changeFloorTo:floor];
        }
        [self.sv setZoomScale:zoomScale + 0.0001f];
    }
}

- (IBAction)folowing:(id)sender {
  if (enableFollow){
    dynamicModeTimer = nil;
    [_rotateButton setImage:[UIImage imageNamed:@"btnDynamicMap"] forState:UIControlStateNormal];
    self.rotateButton.transform = CGAffineTransformMakeRotation(M_PI/4.);
    enableFollow = NO;
    zoomScale = _sv.zoomScale;
    [self.sv setZoomScale:zoomScale - 0.0001f];
  }
  else{
    if(res.ErrorCode){
      [self addRouteErrorViewWithTitle:@"I can't detect your position.              You are out of range of navigation"];
      errorViewType = ErrorViewTypeNavigation;
//      routeErrorView.hidden = NO;
      errorViewTimer = [NSTimer scheduledTimerWithTimeInterval:5.f
                                                        target:self
                                                      selector:@selector(dismissRouteErrorView:)
                                                      userInfo:nil
                                                       repeats:NO];
    }
    else{
      if(errorViewType == ErrorViewTypeNavigation){
        routeErrorView.hidden = YES;
        routeErrorView = nil;
      }
      [_rotateButton setImage:[UIImage imageNamed:@"btnDynamicMap"] forState:UIControlStateNormal];
      self.rotateButton.transform = CGAffineTransformMakeRotation(0.f);
      enableFollow = YES;
      zoomScale = _sv.zoomScale;
      if (sublocationId != res.outSubLocation){
        NSUInteger floor = [self.mapHelper.sublocId indexOfObject:[NSNumber numberWithInteger:res.outSubLocation]];
        self.btnUpFloor.alpha = 1.f;
        self.btnDownFloor.alpha = 1.f;
        if(floor == 0)
          self.btnDownFloor.alpha = 0.7f;
        if(floor == self.mapHelper.sublocId.count - 1)
          self.btnUpFloor.alpha = 0.7f;
        self.mapHelper.floor = floor;
        [self changeFloorTo:floor];
      }
      [self.sv setZoomScale:zoomScale + 0.0001f];
    }
  }
}

#pragma mark - MapHelperDelegate

- (void) startNavigation{
  if (self.mapHelper.sublocId.count == 1){
    self.btnDownFloor.hidden = YES;
    self.btnUpFloor.hidden = YES;
    self.txtFloor.hidden = YES;
  }
  else{
    self.btnDownFloor.alpha = 0.7f;
  }
}

- (void) stopNavigation{
  current.hidden = YES;
  enableFollow = NO;
}

- (void) changeCoordinates{
    
  [self movePositionWithZoom:NO];
}

#pragma mark - NavigineManagerStepsDelegate

-(void) updateSteps:(NSNumber *)numberOfSteps with:(NSNumber *)distance{
  //  NSString *text = [NSString stringWithFormat:@"iOS:%@ distance:%.2lf",numberOfSteps, [distance floatValue]];
  //  self.iOSPedometer.text = text;
}

- (void) yawCalculatedByIos:(double)yaw{
  //  yawByIos = yaw;
}

#pragma mark UIWebViewDelegate methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
  return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
//  NSString *jsCommand = [NSString stringWithFormat:@"document.body.style.zoom = %lf;",self.image.scale];
    NSString *jsCommand = [NSString stringWithFormat:@"document.body.style.margin='0';document.body.style.padding = '0';document.body.style.zoom = %lf;",self.image.scale];
  [webView stringByEvaluatingJavaScriptFromString:jsCommand];
  //  [self.sv addSubview:contentView];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end