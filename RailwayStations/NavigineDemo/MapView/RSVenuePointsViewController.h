//
//  RSVenuePointsViewController.h
//  RailwayStations
//
//  Created by Artem Peskishev on 01.04.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigineSDK.h"
#import "MapHelper.h"

@interface RSVenuePointsViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,MapViewDelegate>
@property NSArray *venuePoints;
@property Categories *category;
@end
