//
//  RSVenueTableViewCell.m
//  RailwayStations
//
//  Created by Artem Peskishev on 01.04.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSVenueTableViewCell.h"

@implementation RSVenueTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
