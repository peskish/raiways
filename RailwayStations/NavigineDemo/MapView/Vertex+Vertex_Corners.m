//
//  Vertex+Vertex_Corners.m
//  RailwayStations
//
//  Created by Artem Peskishev on 11.04.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "Vertex+Vertex_Corners.h"
#import <objc/runtime.h>

static void const *key;

@implementation Vertex (Vertex_Corners)
@dynamic cornerType;

-(void)setCornerType:(NSString *)cornerType
{
    objc_setAssociatedObject(self, key, cornerType, OBJC_ASSOCIATION_RETAIN);
}

-(NSString *)cornerType
{
    return objc_getAssociatedObject(self, key);
}

@end

