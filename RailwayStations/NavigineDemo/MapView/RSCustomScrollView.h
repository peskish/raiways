//
//  RSCustomScrollView.h
//  RailwayStations
//
//  Created by Artem Peskishev on 04.04.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSCustomScrollView : UIScrollView

@end
