//
//  RSCustomScrollView.m
//  RailwayStations
//
//  Created by Artem Peskishev on 04.04.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSCustomScrollView.h"

@implementation RSCustomScrollView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.delaysContentTouches = NO;
    }
    
    return self;
}

- (BOOL)touchesShouldCancelInContentView:(UIView *)view {
    if ([view isKindOfClass:UIButton.class]) {
        return YES;
    }
    
    return [super touchesShouldCancelInContentView:view];
}
@end
