//
//  Vertex+Vertex_Corners.h
//  RailwayStations
//
//  Created by Artem Peskishev on 11.04.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "Vertex.h"

@interface Vertex (Vertex_Corners)
@property (nonatomic, assign) NSString *cornerType;
@end
