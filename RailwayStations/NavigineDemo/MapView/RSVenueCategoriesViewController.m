//
//  RSVenuePointsViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 31.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSVenueCategoriesViewController.h"
#import "NavigineSDK.h"
#import "Category.h"
#import "RSCategoryTableViewCell.h"
#import "RSVenuePointsViewController.h"
#import "RSVenueTableViewCell.h"
#import "MapViewController.h"

@interface RSVenueCategoriesViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *filteredArray;
@property NSMutableArray *searchArray;
@property Categories *selectedCategory;
@property BOOL isSearch;
@property (nonatomic, strong) MapHelper *mapHelper;

@end

Venue *venues2;
NSString *type2;

@implementation RSVenueCategoriesViewController



- (void)viewDidLoad {
    [super viewDidLoad];

    self.searchArray = [NSMutableArray new];
    self.filteredArray = [NSMutableArray new];
    
    [self setDefaultData];
}

- (void)setDefaultData
{
    [self.filteredArray removeAllObjects];
    
    for (Categories *catg in self.categories)
    {
        [catg.venues removeAllObjects];
    }
    
    for (Categories *catg in self.categories)
    {
        for (Venue *ven in self.venuePoints)
        {
            if (ven.category == catg.id)
            {
                [catg.venues addObject:ven];
            }
        }
        
        if (catg.venues.count > 0)
        {
            [self.filteredArray addObject:catg];
        }
        
    }
    
    Categories *category = [[Categories alloc]init];
    
    category.id = 0;
    category.nameRu = @"Мое местоположение";
    
    Categories *category2 = [[Categories alloc]init];

    category2.id = 999;
    category2.nameRu = @"VIP сопровождение";
   
    [self.filteredArray insertObject:category atIndex:0];
    [self.filteredArray insertObject:category2 atIndex:1];
    
    [self.tableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isSearch) return self.searchArray.count;
    return self.filteredArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isSearch)
    {
        self.mapHelper = [MapHelper sharedInstance];
        self.mapHelper.venueDelegate = self;
        // dequeue a custom table view cell subclass
        RSVenueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"venueCell"
                                                                     forIndexPath:indexPath];
        Venue *cat = self.searchArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell.nameLabel setText:cat.name];
        
        return cell;
    } else {
        // dequeue a custom table view cell subclass
        RSCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"categoryCell"
                                                                                 forIndexPath:indexPath];
        Categories *cat = self.filteredArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell setCellWith:cat];
        
        return cell;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];

    if (self.isSearch)
    {
        type2 = @"route";
        venues2 = self.searchArray[indexPath.row];
        
        MapViewController *mapVC = (MapViewController *)self.parentViewController;
        [mapVC showVenue];

    } else {
        if (indexPath.row == 0)
        {
            MapViewController *mapVC = (MapViewController *)self.parentViewController;
            mapVC.showList = NO;
            [mapVC findUser];
            return;
        }
        self.selectedCategory = self.filteredArray[indexPath.row];
        
        if ([self.selectedCategory.nameRu isEqualToString:@"VIP сопровождение"])
        {
            [self performSegueWithIdentifier:@"showCategoryContent" sender:self];
        } else {
            [self performSegueWithIdentifier:@"openCategory" sender:self];
        }
        
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"openCategory"])
    {
        RSVenuePointsViewController *venueVC = segue.destinationViewController;
        venueVC.venuePoints = [self.selectedCategory.venues allObjects];
        venueVC.category = self.selectedCategory;
    } else if ([segue.identifier isEqualToString:@"showCategoryContent"])
    {
        
    }
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)textfieldEditingChanged:(id)sender {
    UITextField *textField = sender;
    [self.searchArray removeAllObjects];
    
    if (textField.text.length == 0)
    {
        self.isSearch = NO;
        [self setDefaultData];
        [self.tableView reloadData];
    } else {
        self.isSearch = YES;
        for (Categories *catg in self.categories)
        {
            for (Venue *ven in catg.venues)
            {
                if ([[ven.name lowercaseString] rangeOfString:[textField.text lowercaseString]].location == NSNotFound)
                {
                } else {
                    [self.searchArray addObject:ven];
                }
            }
            
        }
        [self.tableView reloadData];
    }
}

- (Venue *)routeToPlace {
    return venues2;
}

- (NSString *)showType {
    return type2;
}


@end
