//
//  RSAudioHelperManager.h
//  RailwayStations
//
//  Created by Artem Peskishev on 11.04.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface RSAudioHelperManager : NSObject
@property SystemSoundID sound1;

+ (RSAudioHelperManager *)sharedManager;

- (void)play:(NSString *)cornerType;

@end
