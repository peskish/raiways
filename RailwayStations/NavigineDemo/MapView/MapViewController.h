//
//  MapView.h
//  NavigineDemo
//
//  Created by Administrator on 7/14/14.
//  Copyright (c) 2014 Navigine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigineManager.h"
#import <UIKit/UIGestureRecognizer.h>
#import "PlaceView.h"
#import "CustomTabBarViewController.h"
#import "MapHelper.h"
#import "DebugHelper.h"
#import "MapPin.h"
#import "PressPin.h"
#import "PositionOnMap.h"
#import "RSCustomScrollView.h"
#import "Vertex+Vertex_Corners.h"


typedef enum {
  DistanceInMinutes = 0,
  DistanceInMeters
} DistanceType;


typedef enum {
  RouteTypeNone = 0,
  RouteTypeFromIcon ,
  RouteTypeFromClick
} RouteType;

typedef enum{
  ErrorViewTypeNone = 0,
  ErrorViewTypeNavigation,
  ErrorViewTypeNewRoute,
  ErrorViewTypeNoGraph
}ErrorViewType;


@interface MapViewController : UIViewController <UIScrollViewDelegate, UIWebViewDelegate, MapHelperDelegate, NavigineManagerStepsDelegate, UIGestureRecognizerDelegate,NavigineCoreDelegate,LoaderHelperDelegate>{
}

@property (nonatomic, weak) NSObject <LoaderHelperDelegate> *loaderHelperDelegate;
@property (nonatomic, weak) NSObject <NavigineCoreDelegate> *delegate;

@property (weak, nonatomic) IBOutlet RSCustomScrollView *sv;
@property (weak, nonatomic) IBOutlet UIButton *rotateButton;
@property (weak, nonatomic) IBOutlet UIButton *zoomInBtn;
@property (weak, nonatomic) IBOutlet UIButton *zoomOutBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnDownFloor;
@property (weak, nonatomic) IBOutlet UIButton *btnUpFloor;
@property (weak, nonatomic) IBOutlet UILabel *txtFloor;
@property (weak, nonatomic) IBOutlet UILabel *iOSPedometer;
@property (weak, nonatomic) IBOutlet UILabel *naviginePedometer;
@property (weak, nonatomic) IBOutlet UIWebView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *rotateView;
@property (weak, nonatomic) IBOutlet UIImageView *progressBar;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property BOOL showList;
- (void)findUser;
- (void)showVenue;
- (void)showAnnotationForMapPin:(MapPin *)mappin;
- (void)selectPinWithVenue:(Venue *)v;
- (IBAction)zoomInTouch:(id)sender;
- (IBAction)zoomOutTouch:(id)sender;
- (IBAction)upFloor:(id)sender;
- (IBAction)downFloor:(id)sender;
@end
