//
//  RSEmergencyViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 24.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSEmergencyViewController.h"
#import "RSCallAndNavigateTableViewCell.h"


@interface RSEmergencyViewController ()
@property (nonatomic, strong) NSArray *arrayOfMenuItems;
@end

@implementation RSEmergencyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"shevrone.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"Ленинградский вокзал" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"GothamPro-Light" size:13]];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 200, 32)];
    [backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    negativeSpacer.width = -25;
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer, backButtonItem] animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"EmergencyItems" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:jsonPath];
    NSError *error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    self.arrayOfMenuItems = json[@"items"];
}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayOfMenuItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // dequeue a custom table view cell subclass
    RSCallAndNavigateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"callAndNavigateCell"
                                                                             forIndexPath:indexPath];
    
    NSDictionary *menuItemDict = self.arrayOfMenuItems[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.titleLabel setText:[menuItemDict valueForKey:@"name"]];
    
    switch (indexPath.row) {
        case 0:
            cell.phoneNumber = @"8(499)262-60-38";
            break;
        case 1:
            cell.phoneNumber = @"8(499)260-26-66";
            break;
        case 2:
            cell.phoneNumber = @"8(800)775-00-00";
            break;
        case 3:
            cell.phoneNumber = @"8(499)260-23-38";
            break;
        case 4:
            cell.phoneNumber = @"8(499)260-26-49";
            break;
            
        default:
            break;
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
