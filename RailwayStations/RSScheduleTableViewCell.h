//
//  RSScheduleTableViewCell.h
//  RailwayStations
//
//  Created by Artem Peskishev on 27.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSScheduleTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fromLabel;
@property (weak, nonatomic) IBOutlet UILabel *destinLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *railNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *trainNoLabel;
@property (weak, nonatomic) IBOutlet UIButton *scheduleButton;
@property NSDictionary *dictWithInfo;
@property (weak, nonatomic) IBOutlet UIButton *starButton;

-(void)setCellWithDict:(NSDictionary *)dict;


@end
