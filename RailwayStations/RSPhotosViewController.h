//
//  RSPhotosViewController.h
//  RailwayStations
//
//  Created by Artem Peskishev on 28.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSPhotosViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>

@end
