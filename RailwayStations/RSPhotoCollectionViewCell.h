//
//  RSPhotoCollectionViewCell.h
//  RailwayStations
//
//  Created by Artem Peskishev on 28.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSPhotoCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photosImageView;

@end
