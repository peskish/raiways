//
//  RSStationTableViewCell.h
//  RailwayStations
//
//  Created by Artem Peskishev on 23.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSStationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptLabel;

-(void)setCell:(NSDictionary *)dict;

@end
