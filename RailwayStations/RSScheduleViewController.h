//
//  RSScheduleViewController.h
//  RailwayStations
//
//  Created by Artem Peskishev on 27.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSScheduleViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@end
