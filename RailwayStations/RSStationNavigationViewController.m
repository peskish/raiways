//
//  RSStationNavigationViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 25.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSStationNavigationViewController.h"
#import "NavigineSDK.h"
#import "PositionOnMap.h"

@interface RSStationNavigationViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *contentWebView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeight;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property NavigationResults res;
@property NSTimer *refreshTimer;
@property double mapWidth;
@property double mapHeight;
@property PositionOnMap *current;
@end

@implementation RSStationNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.current = [[PositionOnMap alloc] init];
    self.current.backgroundColor = [UIColor blueColor];
    
    self.current.hidden = YES;
    [self.contentView addSubview:self.current];
//    [self.userPosition setImage:[UIImage imageNamed:@"userPosition"]];
//    [self.contentWebView addSubview:self.userPosition];
    self.scrollView.minimumZoomScale=0.5;
    self.scrollView.maximumZoomScale=2.0;
    self.scrollView.delegate=self;

    
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"shevrone.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"Ленинградский вокзал" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"GothamPro-Light" size:13]];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 200, 32)];
    [backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    negativeSpacer.width = -25;
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer, backButtonItem] animated:NO];
    self.navigationItem.hidesBackButton = YES;
    self.refreshTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                      target:self
                                                    selector:@selector(updateLocation)
                                                    userInfo:nil
                                                     repeats:YES];
//    if ([self loadSavedData] != nil)
//    {
//        [self setupWebView:[self loadSavedData]];
//    } else {
        NavigineCore *navigineCore = [NavigineCore defaultCore];
        [navigineCore downloadContent:@"410B-04B0-8E98-A437"
                             location:@"Office"
                          forceReload:NO
                         processBlock:^(NSInteger loadProcess) { NSLog(@"Load process: %zd",loadProcess);
                         } successBlock:^() {
                             [navigineCore startNavigine];
                             NSError *error;
                             NSData *imageData = [[NavigineCore defaultCore] dataForPNGImageAtIndex:0 error:&error];
                             [self saveImageToDisk:imageData];
                             
                             [self setupWebView:imageData];
                             
                         } failBlock:^(NSError *error) {
                             if(error) NSLog(@"ERROR %@",error);
                         }];
//    }
    
}
    


- (void)saveImageToDisk:(NSData *)dataToSave
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths firstObject];
    path = [path stringByAppendingPathComponent:@"myimage.png"];
    NSError *error;
    

    BOOL success = [dataToSave writeToFile:path atomically:YES];
    if(success){
        NSLog(@"Success");
    }else{
        NSLog(@"Error: %@",[error localizedDescription]);
    }
}

- (NSData *)loadSavedData
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths firstObject];
    path = [path stringByAppendingPathComponent:@"myimage.png"];
    NSError *error;
    
    NSData *imageData = [NSData dataWithContentsOfFile:path options:NSDataReadingMappedIfSafe error:nil];
    return imageData;
}

-(void)setupWebView:(NSData *)imageData
{
    UIImage *image = [UIImage imageWithData:imageData];

    self.mapWidth = image.size.width;
    self.mapHeight = image.size.height;
    self.webViewWidth.constant = self.mapWidth;
    self.webViewHeight.constant = self.mapHeight;
    self.scrollView.contentSize = CGSizeMake(self.mapWidth, self.mapHeight);
    
    NSString *html = [NSString stringWithFormat:@"<img src='data:image/png;base64,%@' />", [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
    [self.contentWebView loadHTMLString:html baseURL:nil];

}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.contentWebView;
}

- (void)updateLocation
{
    self.res = [[NavigineCore defaultCore] getNavigationResults];

    if((self.res.X == 0.0 && self.res.Y == 0.0) || self.res.ErrorCode != 0)  {
    
            return;
    }
    
    CGFloat mapWidthInMeter = self.mapWidth;
    CGFloat mapOriginalWidth = (CGFloat)self.contentView.bounds.size.width;
    
    CGFloat xPoint =  self.res.kX * self.mapWidth;
    CGFloat yPoint =  self.res.kY * self.mapHeight;
    
    CGPoint point = CGPointMake(xPoint, yPoint);
    CGFloat xPixInMeter = (CGFloat)mapOriginalWidth/mapWidthInMeter;
    CGFloat yPixInMeter = (CGFloat)mapOriginalWidth/mapWidthInMeter;
    CGRect pointFrame = CGRectMake(-xPixInMeter * self.res.R,-xPixInMeter * self.res.R, 2.0f * xPixInMeter * self.res.R, 2.0f * yPixInMeter * self.res.R);
    
    self.current.hidden = NO;
    self.current.center = point;
    
    [UIView animateWithDuration:1.0/10 animations:^{
        self.current.background.frame = pointFrame;
        self.current.transform= CGAffineTransformMakeRotation((CGFloat)self.res.Yaw);
        self.current.background.layer.cornerRadius = self.current.background.frame.size.height/2.f;
        self.current.center = point;
    }];
}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

//- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
//    
//    [self.userPosition resizePositionOnMapWithZoom:scrollView.zoomScale];
//    if(enableFollow == YES){
//        self.contentView.origin = CGPointMake(0.f, 0.f);
//        [self zoomRectForScrollView:scrollView withScale:scrollView.zoomScale withCenter:CGPointMake(0, 0)];
//    }
//    lineWith = 2.f / scrollView.zoomScale;
//    if (self.sv.zoomScale < 1 && !enableFollow){
//        if ( self.contentView.frame.size.height / self.contentView.frame.size.width > self.sv.frame.size.height / self.sv.frame.size.width){
//            self.contentView.centerX = self.sv.width / 2.f;
//        }
//        else{
//            self.contentView.centerY = self.sv.height / 2.f;
//        }
//    }
//    viewWithNoZoom.frame = self.contentView.frame;
//}

@end
