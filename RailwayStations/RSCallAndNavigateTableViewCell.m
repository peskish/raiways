//
//  RSCallAndNavigateTableViewCell.m
//  RailwayStations
//
//  Created by Artem Peskishev on 24.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSCallAndNavigateTableViewCell.h"

@implementation RSCallAndNavigateTableViewCell



- (IBAction)callButtonTapped:(id)sender {
    NSString *phoneNumber = [@"tel://" stringByAppendingString:self.phoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

@end
