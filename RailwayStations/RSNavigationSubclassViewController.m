//
//  RSNavigationSubclassViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 25.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSNavigationSubclassViewController.h"

@interface RSNavigationSubclassViewController ()
@property NSTimer *timer;
@end

@implementation RSNavigationSubclassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showAlertWithTrain:) name:@"showAlertForTrain" object:nil];
    
    NSDictionary *staredDict = [[NSUserDefaults standardUserDefaults]objectForKey:@"lookingTrackForTrain"];
    if (staredDict)
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:60
                                                      target:self
                                                    selector:@selector(checkForNewInfo:)
                                                    userInfo:staredDict
                                                     repeats:YES];
    }
}

- (void)showAlertWithTrain:(NSNotification *)notification
{
    [self.timer invalidate];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:60
                                                         target:self
                                                       selector:@selector(checkForNewInfo:)
                                                       userInfo:notification.object
                                                        repeats:YES];

}

-(void)checkForNewInfo:(NSTimer *)timer
{
    NSDictionary *trainDict = timer.userInfo;
    NSString *urlAsString = @"http://cms.indigointeractive.ru/rasp/json.php?terminalid=61";
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    NSLog(@"%@", urlAsString);
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (error) {
        } else {
            NSArray * scheduleArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            for (NSDictionary *newTrainDict in scheduleArray)
            {
                if ([newTrainDict[@"TrainNumber"] isEqualToString:trainDict[@"TrainNumber"]])
                {
                    if (![newTrainDict[@"EvTrackNumber"] isEqualToString:trainDict[@"EvTrackNumber"]])
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSString *string = [NSString stringWithFormat:@"Поезд №%@ отправится с пути %@",newTrainDict[@"TrainNumber"],newTrainDict[@"EvTrackNumber"]];
                            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Внимание" message:string delegate:self cancelButtonTitle:@"Ок" otherButtonTitles: nil];
                            [alertView show];
                            [self.timer invalidate];
                            
                        });
                    }
                }
            }

        }
    }];
}

- (BOOL)shouldAutorotate
{
    return [self.visibleViewController shouldAutorotate];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return  [self.visibleViewController shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return [self.visibleViewController supportedInterfaceOrientations];
}

@end
