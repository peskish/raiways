//
//  RSRailwayMainViewController.m
//  RailwayStations
//
//  Created by Artem Peskishev on 23.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSRailwayMainViewController.h"
#import "RSMenuItemWithImageTableViewCell.h"
#import "RSEmergencyViewController.h"
#import "RSWiFiInfoViewController.h"
#import "RSAboutViewController.h"
#import "RSReviewViewController.h"
#import "RSStationNavigationViewController.h"
#import "RSScheduleViewController.h"
#import "RSMobilityHelpViewController.h"
#import "RSAvailiabilityPassportViewController.h"
#import "MapViewController.h"
#import "NavigineManager.h"
#import "RSParkingViewController.h"


@interface RSRailwayMainViewController ()
@property (strong, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic) NSArray *arrayOfMenuItems;
//@property (nonatomic, strong) NavigineManager *navigineManager;

@end

@implementation RSRailwayMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigineManager = [NavigineManager sharedManager];
//    [self.navigineManager startNavigine];
//    [self.navigineManager startRangePushes];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_background.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"shevrone.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"Вернуться к выбору вокзалов" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"GothamPro-Light" size:13]];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 250, 32)];
    [backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];

    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    negativeSpacer.width = -15;
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer, backButtonItem] animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"RailwayMainMenuItems" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:jsonPath];
    NSError *error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    self.arrayOfMenuItems = json[@"items"];
}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayOfMenuItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // dequeue a custom table view cell subclass
    RSMenuItemWithImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainMenuCell"
                                                                   forIndexPath:indexPath];
    
    NSDictionary *menuItemDict = self.arrayOfMenuItems[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    [cell setCellWithDict:menuItemDict];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    switch (indexPath.row) {
        case 0:
        {
            RSAboutViewController * vc = (RSAboutViewController *)[sb instantiateViewControllerWithIdentifier:@"aboutVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 1:
        {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Map" bundle:nil];
            
            MapViewController * vc = (MapViewController *)[sb instantiateViewControllerWithIdentifier:@"stationNavigationVC"];
            vc.showList = YES;
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 2:
        {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Map" bundle:nil];

            MapViewController * vc = (MapViewController *)[sb instantiateViewControllerWithIdentifier:@"stationNavigationVC"];
            vc.showList = NO;
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 3:
        {
            RSScheduleViewController * vc = (RSScheduleViewController *)[sb instantiateViewControllerWithIdentifier:@"scheduleVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 4:
        {
            RSWiFiInfoViewController * vc = (RSWiFiInfoViewController *)[sb instantiateViewControllerWithIdentifier:@"wifiInfoVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
            case 5:
        {
            RSAvailiabilityPassportViewController * vc = (RSAvailiabilityPassportViewController *)[sb instantiateViewControllerWithIdentifier:@"availiabilityPassportVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
            case 6:
        {
            RSMobilityHelpViewController * vc = (RSMobilityHelpViewController *)[sb instantiateViewControllerWithIdentifier:@"mobilityHelpVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 7:
        {
            RSReviewViewController * vc = (RSReviewViewController *)[sb instantiateViewControllerWithIdentifier:@"reviewVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 8:
        {
            //парковки
            
            RSParkingViewController * vc = (RSParkingViewController *)[sb instantiateViewControllerWithIdentifier:@"parkingVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
            break;
        }
        case 9:
        {
            //доставка товаров
            break;
        }
        case 10:
        {
            //купить билет
            NSURL *url = [NSURL URLWithString:@"http://pass.rzd.ru"];
            
            if (![[UIApplication sharedApplication] openURL:url]) {
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
            }
            break;
        }
        case 11:
        {
            RSEmergencyViewController * vc = (RSEmergencyViewController *)[sb instantiateViewControllerWithIdentifier:@"emergencyVC"];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
            
        default:
            break;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
