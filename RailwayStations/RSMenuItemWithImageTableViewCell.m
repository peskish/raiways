//
//  RSMenuItemWithImageTableViewCell.m
//  RailwayStations
//
//  Created by Artem Peskishev on 24.03.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "RSMenuItemWithImageTableViewCell.h"
#define SEPARATOR_HEIGHT 0.5

@implementation RSMenuItemWithImageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0,49,self.frame.size.width,SEPARATOR_HEIGHT)];
    [separatorView setBackgroundColor:[UIColor colorWithRed:0.365 green:0.365 blue:0.365 alpha:1]];
    [self addSubview:separatorView];
}

-(void)setCellWithDict:(NSDictionary *)dict
{
    self.backgroundColor = [UIColor clearColor];
    self.grayChevroneImageView.alpha = 1;

    [self.menuIcon setImage:[UIImage imageNamed:[dict valueForKey:@"icon"]]];
    [self.menuTitle setText:[dict valueForKey:@"name"]];
    [self.menuTitle setFont:[UIFont fontWithName:@"GothamPro-Light" size:17]];
    self.menuIcon.image = [self.menuIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.menuIcon setTintColor:[UIColor colorWithRed:0.47 green:0.47 blue:0.47 alpha:1.00]];
    self.menuTitle.textColor = [UIColor colorWithRed:0.47 green:0.47 blue:0.47 alpha:1.00];
    if ([dict valueForKey:@"needRedBackground"])
    {
        [self.menuTitle setFont:[UIFont fontWithName:@"GothamPro-Bold" size:17]];
        self.menuTitle.textColor = [UIColor whiteColor];
#warning Сделать нормальный шеврон
        self.backgroundColor = [UIColor colorWithRed:0.408 green:0.125 blue:0.137 alpha:1];
        self.grayChevroneImageView.alpha = 0.6f;

    }
    
}

@end
